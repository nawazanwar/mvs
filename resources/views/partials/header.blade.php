<nav class="navbar navbar-expand-sm bg-light navbar-light border-bottom fixed-top top-nav">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navb">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navb">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('home',$request) }}">Home</a>
            </li>
        </ul>
        <div class="form-inline my-2 my-lg-0">
            <a class="btn btn-info btn-sm mx-1" id="syncProductsBtn" onclick="syncVendorProduct();return false;">Sync
                Vendor Products
                <i class="fa fa-asl-interpreting fs-11px" aria-expanded="true"></i>
            </a>
            <a class="btn btn-info btn-sm mx-1" href="{{ route('vendor.create',$request) }}">Create Vendor
                <i class="fa fa-plus-circle fs-11px" aria-expanded="true"></i>
            </a>
        </div>
    </div>
</nav>
