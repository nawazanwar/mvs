<article class="row tab">
    <form method="post" id="vendor_settings_form">
        {{ csrf_field() }}
        <input type="hidden" value="{{$request['store_id']}}" name="store_id">
        <section class="mb-4 col-12">
            <div class="container">
                <h1 class="text-center mb-5">Get Started as Vendor</h1>
                <div class="row">
                    <div class="col-lg-4 pt-3">
                        <h5>Account Settings</h5>
                        <p class="text-muted">
                            Vendor Account settings for suppliers Store.
                        </p>
                    </div>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="email">Store Name</label>
                                    <input type="text" class="form-control" placeholder="Store name" name="name"
                                           id="name">
                                    <small class="text-muted">This Name will see the sellers for products suppliers
                                        name</small>
                                </div>
                                <h6 class="mb-2">Store Activate</h6>
                                <div class="custom-control custom-radio mb-1">
                                    <input type="radio" class="custom-control-input" id="store_active_for_everyone"
                                           value="everyone"
                                           name="active_for" checked>
                                    <label class="custom-control-label" for="store_active_for_everyone">Active for
                                        everyone</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="store_active_for_pre_approval"
                                           value="pre_approval"
                                           name="active_for">
                                    <label class="custom-control-label" for="store_active_for_pre_approval">Pre approval
                                        required for sellers
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="mb-4 col-12">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 pt-3">
                        <h5>Products</h5>
                        <p class="text-muted">Products Settings which are available for sellers</p>
                    </div>
                    <div class="col-lg-8">
                        <div class="card mb-3">
                            <div class="card-body">
                                <h6 class="mb-2">Discount for Sellers</h6>
                                <div class="row mb-3">
                                    <div class="input-group col-md-6">
                                        <input type="number" name="seller_discount"
                                               placeholder="Discount Percentage"
                                               value="0" class="form-control">
                                        <div class="input-group-append">
                                            <span class="input-group-text">%</span>
                                        </div>
                                    </div>
                                    <small class="text-muted col-12">Price for Seller = Price of Product - Discount%
                                    </small>
                                </div>
                                <h6 class="mb-2">Activate Products</h6>
                                <div class="custom-control custom-radio mb-1">
                                    <input type="radio" class="custom-control-input" id="all" value="all"
                                           name="active_products"
                                           onchange="manage_store_activated_products(this);">
                                    <label class="custom-control-label" for="all">All products are Available for
                                        Sellers</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="selected" value="selected"
                                           name="active_products" checked
                                           onchange="manage_store_activated_products(this);">
                                    <label class="custom-control-label" for="selected">Selected Categories are Available
                                        for
                                        Sellers</label>
                                </div>
                            </div>
                            <div id="selected-categories" class="card-footer" style="height: 300px;overflow-y: auto;">
                                <input type="text" id="search_category" onkeyup="search_categories(this)"
                                       placeholder="Search for names.." class="form-control mb-2">
                                <ul id="search_categories_holder" class="list-group">
                                    @foreach($collections as $collKey=>$collValue)
                                        <li class="list-group-item py-2">
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" class="custom-control-input" id="{{ $collKey }}"
                                                       value="{{ $collKey }}" name="active_product_collections[]">
                                                <label class="custom-control-label"
                                                       for="{{$collKey}}">{{$collValue}}</label>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <button class="btn btn-primary" onclick="save_vendor_settings(1);return false;">Save Settings
                        </button>
                        <button class="btn btn-link" onclick="nextPrev(1);return false;">Skip Now</button>
                    </div>
                </div>
            </div>
        </section>
    </form>
</article>
