@extends('layouts.app')
@section('content')
    <style>
        .tab {
            display: none;
        }
    </style>
    <section class="container" id="installation-wrapper">
        @include('installation.term-condition')
        @include('installation.seller-settings')
        @include('installation.vendor-settings')
    </section>
@endsection
@section('pageScript')
    <script>
        function isValidEmailAddress(emailAddress) {
            var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
            return pattern.test(emailAddress);
        }

        function search_categories(cElement) {
            var value = $(cElement).val().toLowerCase();
            $("#search_categories_holder li").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        }

        /**
         * Manage Store Activate Products
         * @param cElement
         */
        function manage_store_activated_products(cElement) {
            if ($(cElement).is(":checked")) {
                var val = $(cElement).val();
                if (val == 'all') {
                    $("#selected-categories").hide();
                } else {
                    $("#selected-categories").show();
                }
            }
        }

        var currentTab = 0;
        showTab(currentTab);

        function showTab(n) {
            var x = $(".tab");
            x[n].style.display = "block";
            if (n == (x.length - 1)) {
                document.getElementById("nextBtn").innerHTML = "Submit";
            } else {
                document.getElementById("nextBtn").innerHTML = "Next";
            }
        }

        function nextPrev(n) {
            var x = document.getElementsByClassName("tab");
            if (n === 1) {
                managePrivacyPolicy(x, n);
            } else if (n === 2) {
                manageRoles(x, n);
            }
        }

        function managePrivacyPolicy(x, n) {
            var email = $("#privacy_email").val();
            if (email == '') {
                toastr.error('Email is Required');
            } else if (!isValidEmailAddress(email)) {
                toastr.error('Please Enter Valid Email');
            } else {
                $.ajax({
                    url: "{{ route('privacy-policy-settings-store') }}",
                    method: 'post',
                    data: $('#term_and_condition_form').serialize(),
                    success: function (response) {
                        if (response.status === true) {
                            x[currentTab].style.display = "none";
                            currentTab = currentTab + n;
                            showTab(currentTab);
                        }
                    }
                });
            }
        }

        /**
         * Save Seller Settings
         * @param n
         */
        function save_seller_settings(n) {
            var x = document.getElementsByClassName("tab");
            $.ajax({
                url: "{{ route('seller-settings-store') }}",
                method: 'post',
                data: $('#seller_settings_form').serialize(),
                success: function (response) {
                    if (response.status === true) {
                        location.reload();
                    }
                }
            });
            return false;
        }

        /**
         * Save Vendor Settings
         * @param n
         */
        function save_vendor_settings(n) {
            var x = document.getElementsByClassName("tab");
            $.ajax({
                url: "{{ route('vendor-settings-store') }}",
                method: 'post',
                data: $('#vendor_settings_form').serialize(),
                success: function (response) {
                    if (response.status === true) {
                        toastr.success('Vendor Settings Saved Successfully');
                        x[currentTab].style.display = "none";
                        currentTab = currentTab + n;
                        showTab(currentTab);
                        window.scrollTo(0, 0);
                    }
                }
            });
            return false;
        }

    </script>
@endsection
