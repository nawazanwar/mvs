<article class="row tab">
    <div class="col-12 text-center">
        <p class="text-uppercase mb-3 mt-2 privacy-title">Verify Your Email Address</p>
    </div>
    <div class="col-12 bg-white card">
        <div class="card-header privacy-header my-3">
            <p class="mb-0"><i class="fas fa-info-circle"></i> Please make sure we have your current email
                address and accept our <a class="text-info">terms
                    and conditions,</a> and <a class="text-info">privacy policy</a></p>
        </div>
        <div class="card-body px-0 pr-0 pt-0">
            <form method="post" id="term_and_condition_form">
                {{ csrf_field() }}
                <input type="hidden" value="{{$request['store_id']}}" name="store_id">
                <div class="form-group">
                    <label>What is your email address?</label>
                    <input type="text" name="email" class="form-control" id="privacy_email">
                </div>
                <div class="form-check form-group">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-check-input w-17 h-17 checkbox" name="term_of_use"
                               value="0">
                        <span class="ml-1">I have read and accept the </span>
                        <a class="text-info">Term of Use</a>
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-check-input w-17 h-17 checkbox" name="privacy_policy"
                               value="0">
                        <span class="ml-1">I have read and accept the </span>
                        <a class="text-info">Privacy Policy</a>
                    </label>
                </div>
            </form>
        </div>
        <div class="card-footer bg-white border-top-0 pt-0 text-right">
            <a class="btn btn-md btn-primary" id="nextBtn" onclick="nextPrev(1)">Next</a>
        </div>
    </div>
</article>
