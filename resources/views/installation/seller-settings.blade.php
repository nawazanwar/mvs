<article class="row tab">
    <section class="mb-4 col-12">
        <div class="container">
            <h3 class="text-left mb-3">Seller Settings</h3>
            <div class="row">
                <div class="col-lg-4 pt-3">
                    <h5>Products</h5>
                    <p class="text-muted">Products Settings</p>
                </div>
                <div class="col-lg-8">
                    <form method="post" id="seller_settings_form">
                        {{ csrf_field() }}
                        <input type="hidden" value="{{$request['store_id']}}" name="store_id">
                        <div class="card mb-3">
                            <div class="card-body">
                                <h6 class="mb-2">Price Margin</h6>
                                <div class="row mb-3">
                                    <div class="input-group col-md-6">
                                        <input type="number" placeholder="Margin Percentage" value="10"
                                               class="form-control" name="price_margin">
                                        <div class="input-group-append">
                                            <span class="input-group-text">%</span>
                                        </div>
                                    </div>
                                    <small class="text-muted col-12">Product Price = Price of Product + Margin%
                                    </small>
                                </div>
                                <h6 class="mb-0">Update Products</h6>
                                <small class="text-muted">Update product info according to vendor products</small>
                                <div class="custom-control custom-checkbox mb-1">
                                    <input type="checkbox" class="custom-control-input" id="activate_product_info_name"
                                           value="name"
                                           name="activate_product_info[]" checked>
                                    <label class="custom-control-label" for="activate_product_info_name">Name</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input"
                                           id="activate_product_info_description"
                                           value="description"
                                           name="activate_product_info[]">
                                    <label class="custom-control-label" for="activate_product_info_description">description</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="activate_product_info_media"
                                           value="media"
                                           name="activate_product_info[]">
                                    <label class="custom-control-label" for="activate_product_info_media">Media</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="activate_product_info_price"
                                           value="price"
                                           name="activate_product_info[]">
                                    <label class="custom-control-label" for="activate_product_info_price">Price</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="activate_product_info_sku"
                                           value="sku"
                                           name="activate_product_info[]">
                                    <label class="custom-control-label" for="activate_product_info_sku">SKU (Stock
                                        Keeping
                                        Unit)</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input"
                                           id="activate_product_info_barcode"
                                           value="barcode"
                                           name="activate_product_info[]" checked>
                                    <label class="custom-control-label" for="activate_product_info_barcode"> Barcode
                                        (ISBN,
                                        UPC, GTIN,
                                        etc.)</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input"
                                           id="activate_product_info_available_quantity" value="available_quantity"
                                           name="activate_product_info[]" checked>
                                    <label class="custom-control-label" for="activate_product_info_available_quantity">Available
                                        Quantity</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="activate_product_info_type"
                                           value="type"
                                           name="activate_product_info[]" checked>
                                    <label class="custom-control-label" for="activate_product_info_type">Type</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input"
                                           id="activate_product_info_vendor"
                                           value="vendor"
                                           name="activate_product_info[]" checked>
                                    <label class="custom-control-label"
                                           for="activate_product_info_vendor">Vendor</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="activate_product_info_tags"
                                           value="tags"
                                           name="activate_product_info[]" checked>
                                    <label class="custom-control-label" for="activate_product_info_tags">Tags</label>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-primary" onclick="save_seller_settings();return false;">Save Settings
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </section>
</article>
