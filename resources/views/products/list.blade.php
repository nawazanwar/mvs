@if(count($products))
    @foreach($products as $product)
        <tr id="cRow_{{ $product->id }}">
            <td class="text-center" style="width: 40px;">
                <input type="checkbox" id="check-{{ $product->id }}" data-id="{{ $product->id }}"
                       value="{{ \App\AppHelper::alreadyAssignedProduct($product)==true?1:0 }}"
                       class="sub_check_product" {{ \App\AppHelper::alreadyAssignedProduct($product)==true?"checked":'' }}>
                <label for="check-{{ $product->id }}"></label>
            </td>
            <td style="width:250px;overflow: hidden;">
                <div class="media">
                    <div class="media-left">
                        <img src="{{ $product->image }}" class="media-object img-thumbnail mr-2" style="width:60px">
                    </div>
                    <div class="media-body">
                        <p class="media-heading">{!! $product->title !!}</p>
                    </div>
                </div>
            </td>
            <td class="text-center">
                {{ $product->product_type }}
            </td>
            <td class="text-center">
                {{ \App\Models\Variant::whereProductId($product->product_id)->whereStoreId($product->store_id)->sum('inventory_quantity') }}
                in stocks
            </td>
            <td class="text-center">
                {{ \App\AppHelper::getVendorName($product->store_id) }}
            </td>
        </tr>
    @endforeach
    <tr>
        <td class="text-center p-3" colspan="12">
            {{ $products->links('pagination::bootstrap-4') }}
        </td>
    </tr>
@else
    <tr>
        <td colspan="20" class="text-center p-4">
            No Product Found
        </td>
    </tr>
@endif
