@extends('layouts.app')
@section('content')
    <section class="container" style="margin-top: 80px;" id="custom-vendor-holder">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <article class="row tab">
                            <form method="POST"
                                  action="{{ route('vendor.store',['store_id'=>$request['store_id']]) }}"
                                  accept-charset="UTF-8"
                                  id="cForm"
                                  @submit.prevent="store();">
                                {{ csrf_field() }}
                                <input type="hidden" value="{{$request['store_id']}}" name="store_id">
                                <section class="mb-4 col-12">
                                    <div class="container">
                                        <div class="row align-items-center">
                                            <div class="col-lg-4 pt-3 align-self-center">
                                                <h5>Account Settings</h5>
                                                <p class="text-muted">
                                                    Vendor Account settings for suppliers Store.
                                                </p>
                                            </div>
                                            <div class="col-lg-8 align-self-center">
                                                <h3 class="text-center mb-3">Get Started as New Vendor</h3>
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div class="form-group">
                                                            <label for="vendor_name">Vendor Name</label>
                                                            <input type="text"
                                                                   class="form-control"
                                                                   required
                                                                   placeholder="Start Writing" name="vendor_name"
                                                                   id="vendor_name">
                                                            <small class="text-muted">Store Name Consider as the Vendor
                                                                Name.This Name will see the sellers for
                                                                products suppliers
                                                                name</small>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="email">Domain</label>
                                                            <input type="text"
                                                                   class="form-control"
                                                                   required
                                                                   placeholder="Store Url without https://"
                                                                   name="domain"
                                                                   id="domain">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="access_token">Access Token</label>
                                                            <input type="text"
                                                                   required
                                                                   class="form-control"
                                                                   placeholder="Start Writing....."
                                                                   name="access_token"
                                                                   id="access_token">
                                                            <small class="text-muted">Your Password Will be Considered
                                                                as the access token</small>
                                                        </div>
                                                        <div class="form-group text-right">
                                                            <button type="submit" class="btn btn-primary" value="Save" id="saveBtn">
                                                                Save <i class='fas fa-save fs-12px'></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </form>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
