@extends('layouts.app')
@section('content')
    <style>
        .toolbar {
            float: left;
        }

        div.dataTables_wrapper div.dataTables_info {
            float: left;
        }

        div.dataTables_wrapper div.dataTables_paginate {
            float: right;
        }

        .toolbar {
            width: 500px;
        }
    </style>
    <section class="container" style="margin-top: 80px !important;">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap"
                               style="width:100%">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Title</th>
                                <th>Type</th>
                                <th>Inventory</th>
                                <th>Vendor</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('pageScript')
    <script>
        function syncVendorProduct() {
            $("#syncProductsBtn").find('i').removeClass('fa fa-asl-interpreting').addClass('fas fa-spinner fa-spin');
            $.ajax({
                url: "{{ route('sync-vendor-products') }}",
                type: 'GET',
                data: 'seller_store_id={{$request['store_id']}}&vendor_store_id=55679615148',
                success: function (response) {
                    $("#syncProductsBtn").find('i').addClass('fa fa-asl-interpreting').removeClass('fas fa-spinner fa-spin');
                }
            });
            return false;
        }

        function syncVendorImages() {
            $.ajax({
                url: "{{ route('sync-vendor-images') }}",
                type: 'GET',
                data: 'seller_store_id={{$request['store_id']}}&vendor_store_id=34815737993',
                success: function (response) {
                    console.log(response);
                }
            });
            return false;
        }

        var myDataTable = null;
        var customSearchObject = {};
        $(document).ready(function () {
            $.fn.dataTable.ext.errMode = 'none';
            myDataTable = $('#datatable').DataTable({
                "serverSide": true,
                "processing": true,
                "paging": true,
                "fixedColumns": true,
                "searching": {"regex": true},
                "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                "pageLength": 50,
                "bSort": false,
                "responsive": true,
                "dom": '<"toolbar">frtip',
                language: {
                    search: "_INPUT_",
                    searchPlaceholder: "Global Search.....",
                    paginate: {
                        next: '&#8594;', // or '→'
                        previous: '&#8592;' // or '←'
                    }
                },
                'ajax': {
                    'url': "{{route('filter-products')}}",
                    'data': function (data) {
                        data.by_vendor = customSearchObject.by_vendor;
                        data.sellerStoreId = "{{ $request['store_id'] }}"
                    }
                },
                columns: [
                    null,
                    /*{data: "id", className: "text-center"},*/
                    {data: 'title'},
                    {data: 'product_type'},
                    {data: 'inventory'},
                    {data: 'vendor'}
                ]
            });
            addToolBar();

            $('.multiselect').multiselect();

        });

        function showHideProductAddBtnBasedOnChildCheckBox() {
            showHideSelectedProducts();
        }

        function showHideProductAddBtnBasedOnParentCheckBox(cElement) {
            if ($(cElement).is(':checked', true)) {
                $(".checkSubProductsCheckBox").prop('checked', true);
            } else {
                $(".checkSubProductsCheckBox").prop('checked', false);
            }
            showHideSelectedProducts();
        }

        function showHideSelectedProducts() {
            var allSelectedProducts = [];
            $(".checkSubProductsCheckBox:checked").each(function () {
                allSelectedProducts.push($(this).attr('id'));
            });
            if (allSelectedProducts.length <= 0) {
                $("#addProductsBtn").hide();
            } else {
                $("#addProductsBtn").show();
            }
        }

        /**
         * Add Selected Vendor Products
         */
        function addSelectedproducts() {
            var productIds = [];
            $(".checkSubProductsCheckBox:checked").each(function () {
                productIds.push($(this).attr('data-id'));
            });
            console.log(productIds);
            $.ajax({
                url: "{{ route('add-selected-vendor-products') }}",
                type: 'POSt',
                data: 'ids=' + productIds.join(",") + '&store_id={{$request['store_id']}}',
                success: function (response) {
                    if (response.status == 'success') {
                        $.success('Product Added Successfully');
                    }
                }
            });
        }

        function addToolBar() {
            var toolBarData = "<section class='row'>";
            var vendors = {!! json_encode(\App\AppHelper::getAllVendors(), JSON_HEX_TAG) !!};
            /* var selectVendors = '<div class="col-md-6 col-lg-6 col-xl-6"> ' +
                 '<select class="form-control form-control-sm multiselect" data-placeholder="Select Vendor"  data-filter="true" onchange="searchByVendor(this);" >' +
                 '<option value="" disabled="disabled" selected="selected">Select Vendor</option>';
             $.each(vendors, function (key, value) {
                 selectVendors += '<option  value=' + value.store_id + '>' + value.name + '</option>';
             });
             selectVendors += '</select></div>';
             toolBarData += selectVendors;*/
            var AddButton = '<div class="col-md-6 col-lg-6 col-xl-6 pt-1"> <a class="btn btn-success hide" id="addProductsBtn" onclick="addSelectedproducts();" >Add Products</a></div>';
            toolBarData += AddButton;
            toolBarData += "</section>";
            $("div.toolbar").html(toolBarData);
        }

        function searchByVendor(cElement) {
            var value = $(cElement).find('option:selected').val();
            customSearchObject['by_vendor'] = value;
            myDataTable.draw();
        }
    </script>
@endsection
