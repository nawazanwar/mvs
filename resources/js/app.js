import Vue from "vue";
import Swal from 'sweetalert2';

window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.Vue = require('vue');
if (document.getElementById("main-holder")) {
    const mainHolder = new Vue({
        el: '#main-holder',
        data: {},
        methods: {
            synVendorProducts: function () {
                alert("yes");
            }
        }
    });
}
if (document.getElementById("custom-vendor-holder")) {
    const vendorHolder = new Vue({
        el: '#custom-vendor-holder',
        data: {},
        methods: {
            store: function () {
                $("#saveBtn").find('i').removeClass('fas fa-save').addClass('fas fa-spinner fa-spin');
                var form = document.getElementById('cForm');
                let data = new FormData(form);
                var action = form.getAttribute('action');
                axios.post(action, data)
                    .then(function (response) {
                        if (response.data.type == 'success') {
                            $("#saveBtn").find('i').addClass('fas fa-save').removeClass('fas fa-spinner fa-spin');
                            Swal.fire({
                                icon: 'success',
                                title: 'Successfully Save Vendor Settings'
                            });
                            setTimeout(function () {
                                location.reload();
                            }, 5000);
                        } else if (response.data.type == 'error') {
                            $("#saveBtn").find('i').addClass('fas fa-save').removeClass('fas fa-spinner fa-spin');
                            Swal.fire({
                                icon: 'error',
                                title: 'Something went Wrong'
                            });
                        } else if (response.data.type == 'already_vendor') {
                            $("#saveBtn").find('i').addClass('fas fa-save').removeClass('fas fa-spinner fa-spin');
                            Swal.fire({
                                icon: 'error',
                                title: 'Vendor with this Domain is Already Exists'
                            });
                        }
                    })
                    .catch(function (error) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Something went Wrong'
                        });
                    });
            }
        }
    });
}
