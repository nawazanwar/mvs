<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFulfillmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fulfillment', function (Blueprint $table) {
            $table->id();
            $table->text('store_id')->nullable();
            $table->text('vendor_store_id')->nullable();
            $table->text('order_id')->nullable();
            $table->text('vendor_order_id')->nullable();
            $table->text('fulfillment_id')->nullable();
            $table->text('vendor_fulfillment_id')->nullable();
            $table->text('location_id')->nullable();
            $table->string('status')->nullable();
            $table->text('shipment_status')->nullable();
            $table->string('service')->nullable();
            $table->text('tracking_company')->nullable();
            $table->string('fulfilment_created_at')->nullable();
            $table->string('fulfilment_updated_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fulfillment');
    }
}
