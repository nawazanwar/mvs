<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMetafieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metafields', function (Blueprint $table) {
            $table->id();
            $table->text('store_id')->nullable();
            $table->text('meta_id')->nullable();
            $table->text('meta_owner_id')->nullable();
            $table->text('meta_owner_resource')->nullable();
            $table->text('meta_namespace')->nullable();
            $table->text('meta_key')->nullable();
            $table->text('meta_value')->nullable();
            $table->text('meta_value_type')->nullable();
            $table->longText('meta_description')->nullable();
            $table->text('meta_created_at')->nullable();
            $table->text('meta_updated_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metafields');
    }
}
