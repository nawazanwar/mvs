<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->id();
            $table->text('store_id')->nullable();
            $table->text('order_id')->nullable();
            $table->text('line_item_id')->nullable();
            $table->text('line_item_product_id')->nullable();
            $table->text('line_item_variant_id')->nullable();
            $table->text('line_item_name')->nullable();
            $table->string('line_item_quantity')->nullable();
            $table->text('line_item_sku')->nullable();
            $table->text('line_item_vendor')->nullable();
            $table->string('line_item_fulfillment_service')->nullable();
            $table->string('line_item_requires_shipping')->nullable();
            $table->string('line_item_taxable')->nullable();
            $table->string('line_item_grams')->nullable();
            $table->string('line_item_price')->nullable();
            $table->string('line_item_total_discount')->nullable();
            $table->string('line_item_fulfillment_status')->nullable();
            $table->string('line_item_fulfillable_quantity')->nullable();
            $table->string('line_item_product_exists')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
    }
}
