<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCollectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collections', function (Blueprint $table) {
            $table->id();
            $table->text('store_id')->nullable();
            $table->text('collection_id')->nullable();
            $table->text('title')->nullable();
            $table->text('handle')->nullable();
            $table->longText('body_html')->nullable();
            $table->text('sort_order')->nullable();
            $table->text('template_suffix')->nullable();
            $table->text('published_scope')->nullable();
            $table->longText('image')->nullable();
            $table->text('disjunctive')->nullable();
            $table->json('rules')->nullable();
            $table->longText('site_url')->nullable();
            $table->enum('type', ['smart', 'custom'])->default('custom');
            $table->string('collection_updated_at')->nullable();
            $table->string('collection_published_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collections');
    }
}
