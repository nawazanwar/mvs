<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();

            $table->text('store_id')->nullable();
            $table->text('vendor_store_id')->nullable();
            $table->text('product_id')->nullable();
            $table->text('vendor_product_id')->nullable();
            $table->text('title')->nullable();
            $table->text('handle')->nullable();
            $table->text('vendor')->nullable();
            $table->text('product_type')->nullable();
            $table->longText('tags')->nullable();
            $table->longText('body_html')->nullable();
            $table->text('template_suffix')->nullable();
            $table->text('published_scope')->nullable();
            $table->longText('image')->nullable();
            $table->json('images')->nullable();
            $table->text('option1')->nullable();
            $table->text('option2')->nullable();
            $table->text('option3')->nullable();
            $table->json('options')->nullable();
            $table->longText('site_url')->nullable();
            $table->string('product_created_at')->nullable();
            $table->string('product_updated_at')->nullable();
            $table->string('product_published_at')->nullable();
            $table->boolean('is_synced')->default(false);
            $table->boolean('is_synced_images')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
