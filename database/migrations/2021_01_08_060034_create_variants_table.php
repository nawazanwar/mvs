<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('variants', function (Blueprint $table) {
            $table->id();
            $table->text('store_id')->nullable();
            $table->text('vendor_store_id')->nullable();
            $table->text('product_id')->nullable();
            $table->text('vendor_product_id')->nullable();
            $table->text('variant_id')->nullable();
            $table->text('vendor_variant_id')->nullable();
            $table->text('title')->nullable();
            $table->text('sku')->nullable();
            $table->text('price')->nullable();
            $table->text('compare_at_price')->nullable();
            $table->text('position')->nullable();
            $table->text('inventory_policy')->nullable();
            $table->text('fulfillment_service')->nullable();
            $table->text('inventory_management')->nullable();
            $table->text('option1')->nullable();
            $table->text('option2')->nullable();
            $table->text('option3')->nullable();
            $table->text('taxable')->nullable();
            $table->text('barcode')->nullable();
            $table->text('weight')->nullable();
            $table->text('weight_unit')->nullable();
            $table->text('inventory_item_id')->nullable();
            $table->text('inventory_quantity')->nullable();
            $table->text('old_inventory_quantity')->nullable();
            $table->text('requires_shipping')->nullable();
            $table->longText('image')->nullable();
            $table->text('variant_created_at')->nullable();
            $table->text('variant_updated_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('variants');
    }
}
