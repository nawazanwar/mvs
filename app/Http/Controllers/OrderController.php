<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\OrderBilling;
use App\Models\OrderCustomer;
use App\Models\Fulfillment;
use App\Models\FulfilmentLineItem;
use App\Models\OrderItem;
use App\Models\OrderShipping;
use App\Models\Shopify;
use App\Models\Store;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Create-Order web Hooks
     */
    public function webhookOrderCreate()
    {
        $order = json_decode(file_get_contents('php://input'), true);
        $storeQuery = Store::whereDomain($_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN']);
        if ($storeQuery->exists()) {
            $store = $storeQuery->first();
            $orderId = strval(trim($order['id']));
            $finalArray = Order::manageOrder($order, $store);
            if (count($finalArray) > 0) {
                foreach ($finalArray as $key => $value) {
                    $value['order']['metafields'] = [
                        [
                            'key' => 'seller_store_id',
                            'value' => $store['store_id'],
                            "value_type" => "string",
                            "namespace" => "global"
                        ],
                        [
                            'key' => 'seller_order_id',
                            'value' => $value['order']['id'],
                            "value_type" => "string",
                            "namespace" => "global"
                        ],
                        [
                            'key' => 'seller_order_number',
                            'value' => $value['order']['order_number'],
                            "value_type" => "string",
                            "namespace" => "global"
                        ]
                    ];
                    $value['order']['note_attributes'] = [
                        [
                            'name' => 'Seller Store Name',
                            'value' => $store->store_name
                        ],
                        [
                            'name' => 'Seller Store Domain',
                            'value' => $store->domain
                        ],
                        [
                            'name' => 'Seller Store Order Number',
                            'value' => $value['order']['order_number']
                        ]
                    ];
                    unset($value['order']['order_number']);
                    unset($value['order']['id']);
                    $vendorStoreId = strval(trim($key));
                    $vendorStore = Store::whereStoreId($vendorStoreId)->first();
                    $end_point = "/admin/api/" . env('PUBLIC_APP_API_VERSION') . "/orders.json";
                    Shopify::call($vendorStore->token, $vendorStore->domain, $end_point, $value, 'POST');
                }
            }
            Order::syncMetaFields($store, $orderId);
        }
        return response()->json(['status' => 200]);
    }

    public function webhookOrderUpdate()
    {
        return response()->json(['status' => 200]);
    }
    public function webhookOrderDelete(){
        return response()->json(['status' => 200]);
    }

}
