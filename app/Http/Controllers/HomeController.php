<?php

namespace App\Http\Controllers;

use App\AppHelper;
use App\Models\Collection;
use App\Models\Customer;
use App\Models\Installation;
use App\Models\Order;
use App\Models\Product;
use App\Models\Shopify;
use App\Models\Store;
use App\Models\Variant;
use App\Models\VendorSetting;
use App\Models\WebHook;
use Illuminate\Contracts\Auth\Factory;
use Illuminate\Contracts\Console\Application;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use phpDocumentor\Reflection\Types\Iterable_;
use function GuzzleHttp\Psr7\str;

class HomeController extends Controller
{
    /**
     * @param Request $request
     * @return Application|Factory|View|RedirectResponse
     */
    public function index(Request $request)
    {

        $domain = $request->get('shop', null);
        $hmac = $request->get('hmac', null);
        $timeStamp = $request->get('timestamp', null);
        $storeQuery = Store::whereDomain($domain);
        if ($storeQuery->exists()) {
            $token = $storeQuery->value('token');
            $hooks = json_decode(Shopify::call($token, $domain, '/admin/api/' . env('PUBLIC_APP_API_VERSION') . '/webhooks.json', array("topic" => 'app/uninstalled'), 'GET')['response'], true);
            if (isset($hooks['errors']) && $hooks['errors'] == '[API] Invalid API key or access token (unrecognized login or wrong password)') {
                return redirect()->route('install', [
                    'hmac' => $hmac,
                    'shop' => $domain,
                    'timestamp', $timeStamp
                ]);
            } else {
                if ($request->has('session')) {
                    return $this->manageAppHomePage($request);
                } else {
                    $return_url = 'https://' . $domain . "/admin/apps/" . env('PUBLIC_APP_NAME');
                    header("Location: " . $return_url);
                    die();
                }
            }
        } else {
            return redirect()->route('install', [
                'hmac' => $hmac,
                'shop' => $domain,
                'timestamp', $timeStamp
            ]);
        }

    }

    /**
     * @param Request $request
     */

    public function install(Request $request)
    {
        $storeName = $request->get('shop', null);
        $hMac = $request->get('hmac', null);

        Store::updateOrCreate([
            'domain' => $storeName
        ], [
            'domain' => $storeName,
            'hmac' => $hMac
        ]);

        $install_url = "https://" . $storeName . "/admin/oauth/authorize?client_id=" . env('PUBLIC_APP_API_KEY') . "&scope=" . trim(env('PUBLIC_APP_SCOPES')) . "&redirect_uri=" . urlencode(env('PUBLIC_APP_REDIRECT_URL'));
        header("Location: " . $install_url);
        die();
    }

    public function token(Request $request)
    {
        $params = $_GET;
        $hmac = $_GET['hmac'];
        $params = array_diff_key($params, array('hmac' => ''));
        ksort($params);
        $computed_hmac = hash_hmac('sha256', http_build_query($params), env('PUBLIC_APP_API_SECRET'));
        if (hash_equals($hmac, $computed_hmac)) {
            $query = array(
                "client_id" => env('PUBLIC_APP_API_KEY'),
                "client_secret" => env('PUBLIC_APP_API_SECRET'),
                "code" => $params['code']
            );
            $access_token_url = "https://" . $params['shop'] . "/admin/oauth/access_token";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $access_token_url);
            curl_setopt($ch, CURLOPT_POST, count($query));
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($query));
            $result = curl_exec($ch);
            curl_close($ch);
            $result = json_decode($result, true);
            $token = $result['access_token'];
            $domain = $request->get('shop', null);
            Store::sync($token, $domain);
            $store = Store::whereDomain($domain)->first();
            $store->token = $token;
            if ($store->save()) {
                Store::manageWebHooks($store);
                $return_url = 'https://' . $domain . "/admin/apps/" . env('PUBLIC_APP_NAME');
                header("Location: " . $return_url);
                die();
            }
        } else {
            die('This request is NOT from Shopify!');
        }
    }

    /**
     * @param object $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|View
     */
    public function manageAppHomePage(object $request)
    {
        $request = AppHelper::getAppRequests($request);
        $storeId = strval(trim($request['store_id']));
        $store = Store::whereStoreId($storeId)->first();
        $collections = Installation::getCollections($store);
        $routeType = 'home';
        if (AppHelper::hasRole($store, 'vendor') || AppHelper::hasRole($store, 'seller')) {
            return \view('dashboard.index', compact('request', 'routeType'));
        } else {
            $routeType = 'installation';
            return \view('installation.index', compact('request', 'collections', 'routeType'));
        }
    }
}
