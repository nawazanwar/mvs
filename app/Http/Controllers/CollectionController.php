<?php

namespace App\Http\Controllers;

class CollectionController extends Controller
{
    /**
     * Create Collection - WebHook
     */
    public function webhookCollectionCreate()
    {
        return response()->json(['status' => 200]);
    }

    /**
     * Update Collection - WebHook
     */
    public function webhookCollectionUpdate()
    {
        return response()->json(['status' => 200]);
    }

    /**
     * Delete - Collection WebHook
     */
    public function webhookCollectionDelete()
    {
        return response()->json(['status' => 200]);
    }
}
