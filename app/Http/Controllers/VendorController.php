<?php

namespace App\Http\Controllers;

use App\AppHelper;
use App\Models\Collection;
use App\Models\Customer;
use App\Models\Product;
use App\Models\Role;
use App\Models\SellerVendor;
use App\Models\Shopify;
use App\Models\Store;
use App\Models\Variant;
use App\Models\VendorSetting;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class VendorController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return Application|Factory|View|Response
     */
    public function create(Request $request)
    {

        $request = AppHelper::getAppRequests($request);
        $routeType = 'home';
        return view('dashboard.vendor.create', compact('request', 'routeType'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return array
     */
    public function store(Request $request): array
    {
        $json = array();
        $sellerStoreId = strval(trim($request->input('store_id')));
        $domain = $request->input('domain');
        $token = $request->input('access_token');
        $vendor_name = $request->input('vendor_name');
        $vendorStore = Store::sync($token, $domain);
        Store::manageWebHooks($vendorStore);
        $vendorStoreId = strval(trim($vendorStore['store_id']));
        if (VendorSetting::whereStoreId($vendorStoreId)->exists()) {
            $json['type'] = 'already_vendor';
        } else {
            $settingModel = VendorSetting::updateOrCreate(
                [
                    'store_id' => $vendorStoreId
                ],
                [
                    'store_id' => $vendorStoreId,
                    'name' => $vendor_name,
                    'activate_for' => "pre_approval",
                    'seller_discount' => 0,
                    'active_products' => "all",
                    'active_product_collections' => null
                ]
            );
            if ($settingModel) {
                SellerVendor::updateOrCreate([
                    'seller_store_id' => $sellerStoreId,
                    'vendor_store_id' => $vendorStoreId
                ], [
                    'seller_store_id' => $sellerStoreId,
                    'vendor_store_id' => $vendorStoreId
                ]);
                Role::updateOrCreate(
                    [
                        'store_id' => $vendorStoreId,
                        'name' => 'vendor'
                    ],
                    [
                        'store_id' => $vendorStoreId,
                        'name' => 'vendor'
                    ]
                );
                Product::syncToLocalDb($vendorStore);
                $json['type'] = 'success';
            }
        }
        return $json;
    }
}
