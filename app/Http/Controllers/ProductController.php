<?php

namespace App\Http\Controllers;

use App\AppHelper;
use App\Models\CollectionProduct;
use App\Models\Metafield;
use App\Models\Product;
use App\Models\SellerVendor;
use App\Models\Shopify;
use App\Models\Store;
use App\Models\Variant;
use App\Models\VendorSetting;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * WebHook Create Product
     */
    public function webhookProductCreate()
    {
        $product = json_decode(file_get_contents('php://input'), true);
        $storeQuery = Store::whereDomain($_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN']);
        file_put_contents('create_product_webhook_don_' . $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'] . '.txt', "yes");
        if ($storeQuery->exists()) {
            $store = $storeQuery->first();
            $product_id = strval(trim($product['id']));
            $store_id = strval(trim($store['store_id']));
            Product::manageProduct($product, $store);
            Product::syncMetaFields($store, $product_id);
            Variant::syncMetaFields($store,$product_id);
            if (AppHelper::isVendor($store_id)) {
                $vendorSellers = SellerVendor::where('vendor_store_id', $store_id)->get();
                foreach ($vendorSellers as $vendorSeller) {
                    $sellerStoreId = $vendorSeller->seller_store_id;
                    $sellerStore = Store::where('store_id', $sellerStoreId)->first();
                    Product::createProductInSellerStore($product, $sellerStore, $store);
                }
            }
        }
        return response()->json(['status' => 200]);
    }

    /**
     * WebHook Update Product
     */
    public function webhookProductUpdate()
    {
        $product = json_decode(file_get_contents('php://input'), true);
        $storeQuery = Store::whereDomain($_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN']);
        file_put_contents('update_product_webhook_don_' . $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'] . '.txt', "yes");
        if ($storeQuery->exists()) {
            $store = $storeQuery->first();
            $product_id = strval(trim($product['id']));
            $store_id = strval(trim($store->store_id));
            Product::manageProduct($product, $store);
            if (AppHelper::isVendor($store_id)) {
                $vendorProductsInSellers = Product::where('vendor_store_id', $store_id)
                    ->where('vendor_product_id', $product_id)
                    ->get();
                foreach ($vendorProductsInSellers as $sellerProduct) {
                    $sellerStoreId = strval(trim($sellerProduct->store_id));
                    $sellerStore = Store::whereStoreId($sellerStoreId)->first();
                    Product::updateProductInSellerStore($product, $sellerProduct, $sellerStore);
                }
            }
        }
        return response()->json(['status' => 200]);
    }

    /**
     * WebHook Delete Product
     */
    public function webhookProductDelete()
    {
        return response()->json(['status' => 200]);
    }

    /**
     * Custom collections by Product Id
     * @param $product_id
     * @param $store
     */
    public function getProductCustomCollections($product_id, $store)
    {
        $store_id = strval(trim($store->store_id));
        $last_page = false;
        $params = array('limit' => 250, 'product_id' => $product_id);
        $deleted_items = array();
        while (!$last_page) {
            $end_point = "/admin/api/" . env('PUBLIC_APP_API_VERSION') . "/collects.json";
            $request = Shopify::call($store->token, $store->domain, $end_point, $params, 'GET');
            $header = AppHelper::getShopifyNextPageArray($request['headers']);
            $response = json_decode($request['response'], JSON_PRETTY_PRINT);
            file_put_contents('update_products.txt', print_r($response, true));
            if (isset($response['collects']) && count($response['collects']) > 0) {
                foreach ($response['collects'] as $collect) {
                    $product_id = strval(trim($collect['product_id']));
                    $collection_id = strval(trim($collect['collection_id']));
                    CollectionProduct::updateOrCreate([
                        'collection_id' => $collection_id,
                        'product_id' => $product_id,
                        'store_id' => $store_id
                    ], [
                        'collection_id' => $collection_id,
                        'product_id' => $product_id,
                        'store_id' => $store_id
                    ]);
                    array_push($deleted_items, $collection_id);
                }
            }
            if (isset($header['next_page'])) {
                $params['page_info'] = $header['next_page'];
            }
            $last_page = $header['last_page'];
        }
        CollectionProduct::whereNotIn('collection_id', $deleted_items)
            ->where('product_id', $product_id)
            ->whereStoreId($store_id)
            ->delete();
    }

    /**
     * Custom collections by Product Id
     * @param $product_id
     * @param $store
     */
    public function getProductSmartCollections($product_id, $store)
    {
        $store_id = strval(trim($store->store_id));
        $last_page = false;
        $params = array('limit' => 250, 'product_id' => $product_id);
        $deleted_items = array();
        while (!$last_page) {
            $end_point = "/admin/api/" . env('PUBLIC_APP_API_VERSION') . "/smart_collections.json";
            $request = Shopify::call($store->token, $store->domain, $end_point, $params, 'GET');
            $header = AppHelper::getShopifyNextPageArray($request['headers']);
            $response = json_decode($request['response'], JSON_PRETTY_PRINT);
            if (isset($response['smart_collections']) && count($response['smart_collections']) > 0) {
                foreach ($response['smart_collections'] as $smart_collection) {
                    $collection_id = strval(trim($smart_collection['id']));
                    CollectionProduct::updateOrCreate([
                        'collection_id' => $collection_id,
                        'product_id' => $product_id,
                        'store_id' => $store_id
                    ], [
                        'collection_id' => $collection_id,
                        'product_id' => $product_id,
                        'store_id' => $store_id
                    ]);
                    array_push($deleted_items, $collection_id);
                }
            }
            if (isset($header['next_page'])) {
                $params['page_info'] = $header['next_page'];
            }
            $last_page = $header['last_page'];
        }
        CollectionProduct::whereNotIn('collection_id', $deleted_items)
            ->where('product_id', $product_id)
            ->whereStoreId($store_id)
            ->delete();
    }

    /**
     * @param Request $request
     * @return string
     */
    public function filter(Request $request): string
    {
        $sellerStoreId = $request->get('sellerStoreId');
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page
        $search_arr = $request->get('search');
        $records = new Product();
        $records = $records->whereNotNull('vendor_store_id')->whereStoreId($sellerStoreId);
        $searchValue = $search_arr['value']; // Search value
        $totalRecords = $records->select('count(products.*) as allcount')->count();
        $records = $records->where(function ($q) use ($searchValue) {
            $q->where('products.title', 'like', DB::raw("'%$searchValue%'"))
                ->orWhere('products.product_type', 'like', DB::raw("'%$searchValue%'"));
        });

        $totalRecordswithFilter = $records->select('count(products.*) as allcount')->count();

        $records = $records->select(
            'products.*')
            ->orderBy('id', "DESC");
        if ($start !== '0') {
            $records = $records->skip($start);
        }
        $records = $records->take($rowperpage)->get();
        $data_arr = array();
        foreach ($records as $record) {
            $variantModel = Variant::whereStoreId($record->store_id)->whereProductId($record->product_id);
            $vendorModel = VendorSetting::whereStoreId($record->vendor_store_id);
            $data_arr[] = array(
                "DT_RowId" => "row_" . $record->id,
                /*"id" => $link,*/
                "title" => $record->title,
                'product_type' => $record->product_type,
                "inventory" => $variantModel->sum('inventory_quantity') . " in Stock for " . $variantModel->count() . " Variants",
                'vendor' => $vendorModel->value('name')
            );
        }
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data_arr
        );
        echo json_encode($response);
        exit;
    }

    /**
     * Add Selected Vendor Products
     * @param Request $request
     * @return JsonResponse
     */
    public function addSelectedVendorProducts(Request $request): JsonResponse
    {
        $store = Store::whereStoreId($request['store_id']);
        if ($store->exists()) {
            $store = $store->first();
            $ids = explode(',', $request->ids);
            foreach ($ids as $id) {
                $productModel = Product::find($id);
                $productQuery = [
                    "title" => $productModel->title,
                    "body_html" => $productModel->body_html,
                    "vendor" => $productModel->vendor,
                    "product_type" => $productModel->product_type,
                    "published_scope" => "global",
                    'tags' => explode(',', $productModel->tags),
                    'published' => true,
                    "metafields" => [
                        [
                            'key' => 'vendor_store_id',
                            'value' => strval(trim($productModel->store_id)),
                            "value_type" => "string",
                            "namespace" => "global"
                        ],
                        [
                            'key' => 'vendor_product_id',
                            'value' => strval(trim($productModel->product_id)),
                            "value_type" => "string",
                            "namespace" => "global"
                        ]
                    ]
                ];
                /**
                 * Manage Options
                 */
                $optionsArray = array();
                if ($productModel->option1) {
                    $optionsArray[]['name'] = $productModel->option1;
                }
                if ($productModel->option2) {
                    $optionsArray[]['name'] = $productModel->option2;
                }
                if ($productModel->option3) {
                    $optionsArray[]['name'] = $productModel->option3;
                }
                $productQuery['options'] = $optionsArray;
                /**
                 * Manage Variants
                 */
                $variants = Product::getVariants($productModel->store_id, $productModel->product_id);
                $variantArray = array();
                if (count($variants) > 0) {
                    foreach ($variants as $variant) {
                        $variantArray[] = array(
                            'option1' => $variant->option1,
                            'option2' => $variant->option2,
                            'option3' => $variant->option3,
                            'price' => trim($variant->price),
                            'sku' => trim($variant->sku),
                            'barcode' => $variant->barcode,
                            'grams' => $variant->grams,
                            'inventory_quantity' => $variant->inventory_quantity,
                            'inventory_management' => $variant->inventory_management
                        );
                    }
                    $productQuery['variants'] = $variantArray;
                }
                /**
                 * Manage Images
                 */
                $productQuery['images'] = json_decode($productModel->images, TRUE);
                $params = [
                    'product' => $productQuery
                ];
                $end_point = "/admin/api/" . env('PUBLIC_APP_API_VERSION') . "/products.json";
                $request = Shopify::call($store->token, $store->domain, $end_point, $params, 'POST');
                $response = json_decode($request['response'], JSON_PRETTY_PRINT);
                if (isset($response['product']) && count($response['product']) > 0) {
                    return response()->json([
                        'status' => 'success'
                    ]);
                } else {
                    return response()->json([
                        'status' => 'error'
                    ]);
                }
            }
        }
    }
}
