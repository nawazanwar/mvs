<?php

namespace App\Http\Controllers;

use App\Models\PrivacyPolicy;
use App\Models\Role;
use App\Models\SellerSetting;
use App\Models\VendorSetting;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    /**
     * Store Privacy Policy
     * @param Request $request
     * @return JsonResponse
     */
    public function storePrivacyPolicy(Request $request): JsonResponse
    {
        $store_id = $request->input('store_id', null);
        $email = $request->input('email', null);
        $term_of_use = $request->input('term_of_use', null);
        $privacy_policy = $request->input('privacy_policy', null);
        $model = PrivacyPolicy::updateOrCreate(
            [
                'store_id' => $store_id
            ],
            [
                'store_id' => $store_id,
                'email' => $email,
                'term_of_use' => $term_of_use,
                'privacy_policy' => $privacy_policy
            ]
        );
        if ($model) {
            return response()->json([
                'status' => true
            ]);
        }
    }

    /**
     * Store Vendor Settings
     * @param Request $request
     * @return JsonResponse
     */
    public function storeVendorSetting(Request $request): JsonResponse
    {
        $store_id = $request->input('store_id', null);
        $name = $request->input('name', null);
        $activate_for = $request->input('activate_for', null);
        $seller_discount = $request->input('seller_discount', null);
        $active_products = $request->input('active_products', null);
        $active_product_collections = $request->input('active_product_collections', null);
        $model = VendorSetting::updateOrCreate(
            [
                'store_id' => $store_id
            ],
            [
                'store_id' => $store_id,
                'name' => $name,
                'activate_for' => $activate_for,
                'seller_discount' => $seller_discount,
                'active_products' => $active_products,
                'active_product_collections' => serialize($active_product_collections)
            ]
        );
        if ($model) {
            Role::updateOrCreate(
                [
                    'store_id' => $store_id,
                    'name' => 'vendor'
                ],
                [
                    'store_id' => $store_id,
                    'name' => 'vendor'
                ]
            );
            return response()->json([
                'status' => true
            ]);
        }
    }

    /**
     * Store Seller Settings
     * @param Request $request
     * @return JsonResponse
     */
    public function storeSellerSetting(Request $request): JsonResponse
    {
        $store_id = $request->input('store_id', null);
        $price_margin = $request->input('price_margin', null);
        $activate_product_info = $request->input('activate_product_info', null);
        $model = SellerSetting::updateOrCreate(
            [
                'store_id' => $store_id
            ],
            [
                'store_id' => $store_id,
                'price_margin' => $price_margin,
                'activate_product_info' => serialize($activate_product_info)
            ]
        );
        if ($model) {
            Role::updateOrCreate(
                [
                    'store_id' => $store_id,
                    'name' => 'seller'
                ],
                [
                    'store_id' => $store_id,
                    'name' => 'seller'
                ]
            );
            return response()->json([
                'status' => true
            ]);
        }
    }
}
