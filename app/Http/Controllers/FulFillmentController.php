<?php

namespace App\Http\Controllers;

use App\AppHelper;
use App\Http\Controllers\Controller;
use App\Models\Fulfillment;
use App\Models\SellerVendor;
use App\Models\Store;
use Illuminate\Http\Request;

class FulFillmentController extends Controller
{
    /**
     * WebHook Create Customers
     */
    public function webhookFulFillmentCreate()
    {
        $fulfilment = json_decode(file_get_contents('php://input'), true);
        $storeQuery = Store::whereDomain($_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN']);
        if ($storeQuery->exists()) {
            $store = $storeQuery->first();
            Fulfillment::manage($fulfilment, $store);
        }
        return response()->json(['status' => 200]);
    }

    /**
     * WebHook Update Customers
     */
    public function webhookFulFillmentUpdate()
    {
        return response()->json(['status' => 200]);
    }

}
