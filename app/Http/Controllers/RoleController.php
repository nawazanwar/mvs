<?php

namespace App\Http\Controllers;

use App\Models\Role;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RoleController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $name = $request->input('role_name', null);
        $store_id = $request->input('store_id', null);
        $model = Role::updateOrCreate(
            [
                'store_id' => $store_id
            ],
            [
                'store_id' => $store_id,
                'name' => $name
            ]
        );
        if ($model) {
            return response()->json([
                'status' => true
            ]);
        }
    }
}
