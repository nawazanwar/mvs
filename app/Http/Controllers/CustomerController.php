<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Metafield;
use App\Models\Store;
use Cassandra\Custom;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * WebHook Create Customers
     */
    public function webhookCustomerCreate()
    {
        return response()->json(['status' => 200]);
    }

    /**
     * WebHook Update Customers
     */
    public function webhookCustomerUpdate()
    {
        return response()->json(['status' => 200]);
    }

    /**
     * WebHook Update Customers
     */
    public function webhookCustomerDelete()
    {
        return response()->json(['status' => 200]);
    }
}
