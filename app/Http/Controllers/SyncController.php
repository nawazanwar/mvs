<?php

namespace App\Http\Controllers;

use App\AppHelper;
use App\Models\Collection;
use App\Models\Customer;
use App\Models\Order;
use App\Models\Product;
use App\Models\Shopify;
use App\Models\Store;
use App\Models\Variant;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class SyncController extends Controller
{
    /**
     * @param Request $request
     */
    public function vendorProducts(Request $request)
    {
        set_time_limit(0);
        $sellerStoreId = $request->seller_store_id;
        $vendorStoreId = $request->vendor_store_id;
        $sellerStore = Store::whereStoreId($sellerStoreId)->first();
        $productsQuery = Product::whereStoreId($vendorStoreId)->where('is_synced', false);
        if ($productsQuery->exists()) {
            $products = $productsQuery->get();
            foreach ($products as $productModel) {
                $productQuery = [
                    "title" => $productModel->title,
                    "body_html" => $productModel->body_html,
                    "vendor" => $productModel->vendor,
                    "product_type" => $productModel->product_type,
                    'tags' => explode(',', $productModel->tags),
                    "published_scope" => "global",
                    'status' => 'draft',
                    "metafields" => [
                        [
                            'key' => 'vendor_store_id',
                            'value' => strval(trim($productModel->store_id)),
                            "value_type" => "string",
                            "namespace" => "global"
                        ],
                        [
                            'key' => 'vendor_product_id',
                            'value' => strval(trim($productModel->product_id)),
                            "value_type" => "string",
                            "namespace" => "global"
                        ]
                    ]
                ];
                /**
                 * Manage Options
                 */
                $optionsArray = array();
                if ($productModel->option1) {
                    $optionsArray[]['name'] = $productModel->option1;
                }
                if ($productModel->option2) {
                    $optionsArray[]['name'] = $productModel->option2;
                }
                if ($productModel->option3) {
                    $optionsArray[]['name'] = $productModel->option3;
                }
                $productQuery['options'] = $optionsArray;
                /**
                 * Manage Variants
                 */
                $variants = Product::getVariants($productModel->store_id, $productModel->product_id);
                $variantArray = array();
                if (count($variants) > 0) {
                    foreach ($variants as $variantModel) {
                        $variantArray[] = array(
                            'option1' => $variantModel->option1,
                            'option2' => $variantModel->option2,
                            'option3' => $variantModel->option3,
                            'price' => trim($variantModel->price),
                            'sku' => trim($variantModel->sku),
                            'barcode' => $variantModel->barcode,
                            'grams' => $variantModel->grams,
                            'inventory_quantity' => $variantModel->inventory_quantity,
                            'inventory_management' => $variantModel->inventory_management,
                            "metafields" => [
                                [
                                    'key' => 'vendor_store_id',
                                    'value' => strval(trim($productModel->store_id)),
                                    "value_type" => "string",
                                    "namespace" => "global"
                                ],
                                [
                                    'key' => 'vendor_product_id',
                                    'value' => strval(trim($productModel->product_id)),
                                    "value_type" => "string",
                                    "namespace" => "global"
                                ],
                                [
                                    'key' => 'vendor_variant_id',
                                    'value' => strval(trim($variantModel->variant_id)),
                                    "value_type" => "string",
                                    "namespace" => "global"
                                ]
                            ]
                        );
                    }
                    $productQuery['variants'] = $variantArray;
                }
                /**
                 * Manage Images
                 */
                if (!is_null($productModel->images)) {
                    $images = json_decode($productModel->images, TRUE);
                    $finalImagesArray = array();
                    if (count($images)) {
                        foreach ($images as $image) {
                            $finalImagesArray[] = [
                                'src' => $image['src'],
                                'alt' => implode(',', $image['variant_ids'])
                            ];
                            $productQuery['images'] = $finalImagesArray;
                        }
                    }
                }
                $params = [
                    'product' => $productQuery
                ];
                $end_point = "/admin/api/" . env('PUBLIC_APP_API_VERSION') . "/products.json";
                $request = Shopify::call($sellerStore->token, $sellerStore->domain, $end_point, $params, 'POST');
                $response = json_decode($request['response'], JSON_PRETTY_PRINT);
                if (isset($response['product']) && count($response['product']) > 0) {
                    $productModel->is_synced = true;
                    $productModel->save();
                } else {
                    print_r($response);
                }
            }
        }
    }
}
