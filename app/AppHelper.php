<?php


namespace App;


use App\Models\GApi;
use App\Models\Product;
use App\Models\Role;
use App\Models\SellerSetting;
use App\Models\Sheet;
use App\Models\Store;
use App\Models\StoreSheet;
use App\Models\Variant;
use App\Models\VendorSetting;

class AppHelper
{
    /**
     * @param $request
     * @return array
     */
    public static function getAppRequests($request): array
    {
        $requestArray = array();
        $requestArray['hmac'] = $request->get('hmac', null);
        $requestArray['locale'] = $request->get('locale', null);
        $requestArray['new_design_language'] = $request->get('new_design_language', null);
        $requestArray['session'] = $request->get('session', null);
        $requestArray['shop'] = $request->get('shop', null);
        $requestArray['timestamp'] = $request->get('timestamp', null);
        $domain = $request->get('shop', null);
        $store = Store::whereDomain($domain)->first();
        $requestArray['store_id'] = $store->store_id;
        $requestArray['domain'] = $store->domain;
        $requestArray['token'] = $store->token;
        return $requestArray;
    }

    /**
     * Get the next page Array
     * @param $header
     * @return array
     */
    public static function getShopifyNextPageArray($header): array
    {
        $response = array();
        if (isset($header['link'])) {
            $links = explode(',', $header['link']);
            foreach ($links as $link) {
                if (strpos($link, 'rel="next"')) {
                    preg_match('~<(.*?)>~', $link, $next);
                    $url_components = parse_url($next[1]);
                    parse_str($url_components['query'], $params);
                    $response['next_page'] = $params['page_info'];
                    $response['last_page'] = false;
                } else {
                    $response['last_page'] = true;
                }
            }
        } else {
            $response['last_page'] = true;
        }
        return $response;
    }

    /**
     * Check if Has Role...
     * @param $store
     * @param $name
     * @return mixed
     */
    public static function hasRole($store, $name)
    {
        return Role::whereStoreId($store->store_id)->whereName($name)->exists();
    }

    /**
     * All Vendors....
     * @return mixed
     */
    public static function pluckAllVendors()
    {
        return VendorSetting::select('store_id')->get()->toArray();
    }

    /**
     * All Vendors....
     * @return mixed
     */
    public static function getAllVendors()
    {
        return VendorSetting::all();
    }

    /**
     * Get Vendor Name
     * @param $store_id
     * @return mixed
     */
    public static function getVendorName($store_id)
    {
        return VendorSetting::whereStoreId($store_id)->value('name');
    }

    /**
     * @param $vendorProductId
     * @param $vendorStoreId
     * @param $sellerStoreId
     * @return mixed
     */
    public static function alreadyAssignedProduct($vendorProductId, $vendorStoreId, $sellerStoreId)
    {
        return Product::where('vendor_product_id', $vendorProductId)->where('vendor_store_id', $vendorStoreId)->where('store_id', $sellerStoreId)->exists();
    }

    /**
     * @param $productId
     * @param $storeId
     * @return mixed
     */
    public static function isVendorProduct($productId, $storeId)
    {
        return Product::whereProductId($productId)->whereStoreId($storeId)->whereNotNull('vendor_store_id')->whereNotNull('vendor_product_id')->exists();
    }

    public static function getVendorStoreId($productId, $storeId)
    {
        return Product::whereProductId($productId)->whereStoreId($storeId)->whereNotNull('vendor_store_id')->whereNotNull('vendor_product_id')->value('vendor_store_id');
    }
    public static function getVendorVariantId($productId,$variantId, $storeId)
    {
        return Variant::whereProductId($productId)->whereStoreId($storeId)->whereVariantId($variantId)->value('vendor_variant_id');
    }
    /**
     * @param $storeId
     * @return mixed
     */
    public static function isVendor($storeId)
    {
        return VendorSetting::whereStoreId($storeId)->exists();
    }

    /**
     * @param $storeId
     * @return mixed
     */
    public static function isSeller($storeId){
        return SellerSetting::whereStoreId($storeId)->exists();
    }

    public static function totalSyncedProducts($storeId)
    {
       return Product::whereStoreId($storeId)->whereNotNull('vendor_store_id')->count();

    }

}
