<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VendorSetting extends Model
{
    use HasFactory;

    protected $fillable = [
        'store_id', 'name', 'active_for', 'seller_discount', 'active_products', 'active_product_collections'
    ];
}
