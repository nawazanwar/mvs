<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Store extends Model
{
    protected $dates = [
        'created_at',
        'updated_at'
    ];
    protected $fillable = [
        'email',
        'store_id',
        'primary_location_id',
        'domain',
        'primary_locale',
        'country',
        'province',
        'city',
        'address1',
        'address2',
        'zip',
        'latitude',
        'longitude',
        'currency',
        'enabled_presentment_currencies',
        'money_format',
        'store_name',
        'store_owner',
        'plan_display_name',
        'plan_name',
        'force_ssl',
        'hmac',
        'token',
        'store_created_at',
        'store_updated_at',
    ];

    /**
     * @param $token
     * @param $domain
     * @return mixed
     */
    public static function sync($token, $domain)
    {
        $end_point = "/admin/api/" . env('PUBLIC_APP_API_VERSION') . "/shop.json";
        $response = Shopify::call($token, $domain, $end_point, null, 'GET');
        $response = json_decode($response['response'], JSON_PRETTY_PRINT);
        if (isset($response['shop']) && count($response['shop']) > 0) {
            $storeObject = $response['shop'];
            $store_id = strval(trim($storeObject['id']));
            $storeModel = Store::updateOrCreate([
                'domain' => $domain
            ], [
                'domain' => $domain,
                'email' => $storeObject['email'],
                'store_id' => $store_id,
                'primary_location_id' => strval(trim($storeObject['primary_location_id'])),
                'primary_locale' => $storeObject['primary_locale'],
                'country' => $storeObject['country'],
                'province' => $storeObject['province'],
                'city' => $storeObject['city'],
                'address1' => $storeObject['address1'],
                'address2' => $storeObject['address2'],
                'zip' => $storeObject['zip'],
                'latitude' => $storeObject['latitude'],
                'longitude' => $storeObject['longitude'],
                'currency' => $storeObject['currency'],
                'enabled_presentment_currencies' => json_encode($storeObject['enabled_presentment_currencies']),
                'money_format' => $storeObject['money_format'],
                'store_name' => $storeObject['name'],
                'store_owner' => $storeObject['shop_owner'],
                'plan_display_name' => $storeObject['plan_display_name'],
                'plan_name' => $storeObject['plan_name'],
                'force_ssl' => $storeObject['force_ssl'],
                'store_created_at' => $storeObject['created_at'],
                'store_updated_at' => $storeObject['updated_at'],
                'token' => $token
            ]);
            self::manageMetaFields($token, $domain, $store_id);
            return $storeModel;
        }

    }

    /**
     * Store - MetaFields
     * @param $token
     * @param $domain
     * @param $store_id
     */
    public static function manageMetaFields($token, $domain, $store_id)
    {
        $meta_field_end_point = "/admin/api/" . env('PUBLIC_APP_API_VERSION') . "/metafields.json";
        $meta_field_response = Shopify::call($token, $domain, $meta_field_end_point, null, 'GET');
        $response = json_decode($meta_field_response['response'], JSON_PRETTY_PRINT);
        if (isset($response['metafields']) && count($response['metafields']) > 0) {
            foreach ($response['metafields'] as $metafield) {
                $meta_id = strval(trim($metafield['id']));
                $owner_id = strval(trim($metafield['owner_id']));
                Metafield::updateOrCreate([
                    'store_id' => $store_id,
                    'meta_id' => $meta_id,
                    'meta_owner_id' => $owner_id
                ], [
                    'store_id' => $store_id,
                    'meta_id' => $meta_id,
                    'meta_owner_id' => $owner_id,
                    'meta_owner_resource' => $metafield['owner_resource'],
                    "meta_namespace" => $metafield['namespace'],
                    'meta_key' => $metafield['key'],
                    'meta_value' => $metafield['value'],
                    'meta_value_type' => $metafield['value_type'],
                    'meta_description' => $metafield['description'],
                    'meta_created_at' => $metafield['created_at'],
                    'meta_updated_at' => $metafield['updated_at']
                ]);
            }
        }
    }

    /**
     * Manage - WebHooks
     * @param $store
     */

    public static function manageWebHooks($store)
    {
        WebHook::createCollection($store);
        WebHook::updateCollection($store);
        WebHook::deleteCollection($store);
        WebHook::createProduct($store);
        WebHook::updateProduct($store);
        WebHook::deleteProduct($store);
        WebHook::createOrder($store);
        WebHook::updateOrder($store);
        WebHook::deleteOrder($store);
        WebHook::createCustomer($store);
        WebHook::updateCustomer($store);
        WebHook::deleteCustomer($store);

        WebHook::createFulfillment($store);
        WebHook::updateFulfillment($store);
    }
}
