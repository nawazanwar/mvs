<?php

namespace App\Models;

use App\AppHelper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use phpDocumentor\Reflection\DocBlock\Tags\See;

class Variant extends Model
{
    use HasFactory;

    protected $table = 'variants';

    /**
     * @var array
     */
    protected $fillable = [
        'store_id',
        'product_id',
        'variant_id',
        'title',
        'sku',
        'price',
        'compare_at_price',
        'position',
        'inventory_policy',
        'fulfillment_service',
        'inventory_management',
        'option1',
        'option2',
        'option3',
        'taxable',
        'barcode',
        'weight',
        'weight_unit',
        'inventory_item_id',
        'inventory_quantity',
        'old_inventory_quantity',
        'requires_shipping',
        'image',
        'variant_created_at',
        'variant_updated_at'
    ];
    protected $dates = [
        'created_at', 'updated_at'
    ];

    /**
     * Manage Product Variants
     * @param $variants
     * @param $store
     */
    public static function manageVariants($variants, $store)
    {
        $store_id = strval(trim($store->store_id));
        foreach ($variants as $variant) {
            $variant_id = strval(trim($variant['id']));
            $product_id = strval(trim($variant['product_id']));
            Variant::updateOrCreate([
                'store_id' => $store_id,
                'variant_id' => $variant_id,
                'product_id' => $product_id
            ], [
                'store_id' => $store_id,
                'variant_id' => $variant_id,
                'product_id' => $product_id,
                'title' => isset($variant['title']) ? $variant['title'] : null,
                'sku' => isset($variant['sku']) ? $variant['sku'] : null,
                'price' => isset($variant['price']) ? $variant['price'] : null,
                'compare_at_price' => isset($variant['compare_at_price']) ? $variant['compare_at_price'] : null,
                'position' => isset($variant['position']) ? $variant['position'] : null,
                'inventory_policy' => isset($variant['inventory_policy']) ? $variant['inventory_policy'] : null,
                'fulfillment_service' => isset($variant['fulfillment_service']) ? $variant['fulfillment_service'] : null,
                'inventory_management' => isset($variant['inventory_management']) ? $variant['inventory_management'] : null,
                'option1' => isset($variant['option1']) ? $variant['option1'] : null,
                'option2' => isset($variant['option2']) ? $variant['option2'] : null,
                'option3' => isset($variant['option3']) ? $variant['option3'] : null,
                'taxable' => isset($variant['taxable']) ? $variant['taxable'] : null,
                'barcode' => isset($variant['barcode']) ? $variant['barcode'] : null,
                'weight' => isset($variant['weight']) ? $variant['weight'] : null,
                'weight_unit' => isset($variant['weight_unit']) ? $variant['weight_unit'] : null,
                'inventory_item_id' => isset($variant['inventory_item_id']) ? strval(trim($variant['inventory_item_id'])) : null,
                'inventory_quantity' => isset($variant['inventory_quantity']) ? $variant['inventory_quantity'] : null,
                'old_inventory_quantity' => isset($variant['old_inventory_quantity']) ? $variant['old_inventory_quantity'] : null,
                'requires_shipping' => isset($variant['requires_shipping']) ? $variant['requires_shipping'] : null,
                'variant_created_at' => isset($variant['created_at']) ? $variant['created_at'] : null,
                'variant_updated_at' => isset($variant['updated_at']) ? $variant['updated_at'] : null
            ]);
        }
    }

    /**
     * Sync Meta Fields
     * @param $store
     * @param null $product_id
     */
    public static function syncMetaFields($store, $product_id = null)
    {
        $store_id = strval(trim($store->store_id));
        if (is_null($product_id)) {
            $model = Variant::whereStoreId($store_id)->get();
        } else {
            $model = Variant::whereStoreId($store_id)->whereProductId($product_id)->get();
        }
        foreach ($model as $item) {
            $model_id = strval(trim($item['variant_id']));
            $last_page = false;
            $params = array('limit' => 250);
            $deleted_items = array();
            while (!$last_page) {
                $end_point = "/admin/api/" . env('PUBLIC_APP_API_VERSION') . "/products/" . $product_id . "/variants/" . $model_id . "/metafields.json";
                $request = Shopify::call($store->token, $store->domain, $end_point, $params, 'GET');
                $header = AppHelper::getShopifyNextPageArray($request['headers']);
                $response = json_decode($request['response'], JSON_PRETTY_PRINT);
                if (isset($response['metafields']) && count($response['metafields']) > 0) {
                    foreach ($response['metafields'] as $metafield) {
                        $meta_id = strval(trim($metafield['id']));
                        $owner_id = strval(trim($metafield['owner_id']));
                        $metaObject = Metafield::updateOrCreate([
                            'store_id' => $store_id,
                            'meta_id' => $meta_id,
                            'meta_owner_id' => $owner_id
                        ], [
                            'store_id' => $store_id,
                            'meta_id' => $meta_id,
                            'meta_owner_id' => $owner_id,
                            'meta_owner_resource' => $metafield['owner_resource'],
                            "meta_namespace" => $metafield['namespace'],
                            'meta_key' => $metafield['key'],
                            'meta_value' => $metafield['value'],
                            'meta_value_type' => $metafield['value_type'],
                            'meta_description' => $metafield['description'],
                            'meta_created_at' => $metafield['created_at'],
                            'meta_updated_at' => $metafield['updated_at']
                        ]);
                        if (strval($metaObject->meta_key) == 'vendor_store_id') {
                            $item->vendor_store_id = strval($metaObject->meta_value);
                            $item->save();
                        }
                        if (strval($metaObject->meta_key) == 'vendor_product_id') {
                            $item->vendor_product_id = strval($metaObject->meta_value);
                            $item->save();
                        }
                        if (strval($metaObject->meta_key) == 'vendor_variant_id') {
                            $item->vendor_variant_id = strval($metaObject->meta_value);
                            $item->save();
                        }
                        array_push($deleted_items, $meta_id);
                    }
                }
                if (isset($header['next_page'])) {
                    $params['page_info'] = $header['next_page'];
                }
                $last_page = $header['last_page'];
            }
            if ($product_id) {
                Metafield::whereNotIn('meta_id', $deleted_items)
                    ->where('meta_owner_id', $product_id)
                    ->where('meta_owner_resource', 'variant')
                    ->whereStoreId($store_id)
                    ->delete();
            }
        }
    }
}
