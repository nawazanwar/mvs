<?php

namespace App\Models;

use App\AppHelper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    use HasFactory;

    protected $table = 'collections';
    /**
     * @var array
     */
    protected $fillable = [
        'store_id',
        'collection_id',
        'title',
        'handle',
        'body_html',
        'sort_order',
        'template_suffix',
        'published_scope',
        'image',
        'disjunctive',
        'rules',
        'site_url',
        'type',
        'collection_updated_at',
        'collection_published_at'
    ];
    protected $dates = [
        'created_at', 'updated_at'
    ];

    /**
     * Sync Smart Collections
     * @param $store
     */
    public static function syncSmartCollections($store)
    {
        $last_page = false;
        $params = array('limit' => 250);
        while (!$last_page) {
            $end_point = "/admin/api/" . env('PUBLIC_APP_API_VERSION') . "/smart_collections.json";
            $request = Shopify::call($store->token, $store->domain, $end_point, $params, 'GET');
            $header = AppHelper::getShopifyNextPageArray($request['headers']);
            $response = json_decode($request['response'], JSON_PRETTY_PRINT);
            if (isset($response['smart_collections']) && count($response['smart_collections']) > 0) {
                foreach ($response['smart_collections'] as $smart_collection) {
                    self::manageCollection($smart_collection, $store, 'smart');
                }
            }
            if (isset($header['next_page'])) {
                $params['page_info'] = $header['next_page'];
            }
            $last_page = $header['last_page'];
        }
    }

    /**
     * Sync Custom Collections
     * @param $store
     */
    public static function syncCustomCollections($store)
    {
        $last_page = false;
        $params = array('limit' => 250);
        while (!$last_page) {
            $end_point = "/admin/api/" . env('PUBLIC_APP_API_VERSION') . "/custom_collections.json";
            $request = Shopify::call($store->token, $store->domain, $end_point, $params, 'GET');
            $header = AppHelper::getShopifyNextPageArray($request['headers']);
            $response = json_decode($request['response'], JSON_PRETTY_PRINT);
            if (isset($response['custom_collections']) && count($response['custom_collections']) > 0) {
                foreach ($response['custom_collections'] as $custom_collection) {
                    self::manageCollection($custom_collection, $store, 'custom');
                }
            }
            if (isset($header['next_page'])) {
                $params['page_info'] = $header['next_page'];
            }
            $last_page = $header['last_page'];
        }
    }

    public static function syncSmartCollectionsProducts($store)
    {
        $store_id = strval(trim($store->store_id));
        $collections = Collection::whereStoreId($store_id)->whereType('smart')->get();
        foreach ($collections as $collection) {
            $collection_id = strval(trim($collection['collection_id']));
            $last_page = false;
            $params = array('limit' => 250, 'collection_id' => $collection_id, 'fields' => 'id');
            while (!$last_page) {
                $end_point = "/admin/api/" . env('PUBLIC_APP_API_VERSION') . "/products.json";
                $request = Shopify::call($store->token, $store->domain, $end_point, $params, 'GET');
                $header = AppHelper::getShopifyNextPageArray($request['headers']);
                $response = json_decode($request['response'], JSON_PRETTY_PRINT);
                if (isset($response['products']) && count($response['products']) > 0) {
                    foreach ($response['products'] as $product) {
                        $product_id = strval(trim($product['id']));
                        CollectionProduct::updateOrCreate([
                            'collection_id' => $collection_id,
                            'product_id' => $product_id,
                            'store_id' => $store_id
                        ], [
                            'collection_id' => $collection_id,
                            'product_id' => $product_id,
                            'store_id' => $store_id
                        ]);
                    }
                }
                if (isset($header['next_page'])) {
                    $params['page_info'] = $header['next_page'];
                }
                $last_page = $header['last_page'];
            }
        }
    }

    /**
     * Sync Custom Collections With Products
     * @param $store
     */
    public static function syncCustomCollectionsProducts($store)
    {
        $store_id = strval(trim($store->store_id));
        $last_page = false;
        $params = array('limit' => 250);
        while (!$last_page) {
            $end_point = "/admin/api/" . env('PUBLIC_APP_API_VERSION') . "/collects.json";
            $request = Shopify::call($store->token, $store->domain, $end_point, $params, 'GET');
            $header = AppHelper::getShopifyNextPageArray($request['headers']);
            $response = json_decode($request['response'], JSON_PRETTY_PRINT);
            if (isset($response['collects']) && count($response['collects']) > 0) {
                foreach ($response['collects'] as $collect) {
                    $product_id = strval(trim($collect['product_id']));
                    $collection_id = strval(trim($collect['collection_id']));
                    CollectionProduct::updateOrCreate([
                        'collection_id' => $collection_id,
                        'product_id' => $product_id,
                        'store_id' => $store_id
                    ], [
                        'collection_id' => $collection_id,
                        'product_id' => $product_id,
                        'store_id' => $store_id
                    ]);
                }
            }
            if (isset($header['next_page'])) {
                $params['page_info'] = $header['next_page'];
            }
            $last_page = $header['last_page'];
        }
    }

    /**
     * Save Smart & Custom Collections
     * @param $collection
     * @param $store
     * @param $type
     * @return mixed
     */
    public static function manageCollection($collection, $store, $type)
    {
        $store_id = strval(trim($store->store_id));
        $collection_id = strval($collection['id']);
        $collectionModel = Collection::updateOrCreate([
            'store_id' => $store_id,
            'collection_id' => $collection_id,
        ], [
            'store_id' => $store_id,
            'collection_id' => $collection_id,
            'title' => isset($collection['title']) ? $collection['title'] : null,
            'handle' => isset($collection['handle']) ? $collection['handle'] : null,
            'body_html' => isset($collection['body_html']) ? $collection['body_html'] : null,
            'sort_order' => isset($collection['sort_order']) ? $collection['sort_order'] : null,
            'template_suffix' => isset($collection['template_suffix']) ? $collection['template_suffix'] : null,
            'published_scope' => isset($collection['published_scope']) ? $collection['published_scope'] : null,
            'disjunctive' => isset($collection['disjunctive']) ? $collection['disjunctive'] : null,
            'site_url' => "https://" . $store->domain . "/collections/" . $collection['handle'],
            'type' => $type,
            'collection_updated_at' => isset($collection['updated_at']) ? $collection['updated_at'] : null,
            'collection_published_at' => isset($collection['published_at']) ? $collection['published_at'] : null
        ]);
        if (isset($collection['image']) && count($collection['image'])) {
            $collectionModel->image = isset($collection['image']['src']) ? $collection['image']['src'] : null;
        } else {
            $collectionModel->image = null;
        }
        if (isset($collection['rules']) && count($collection['rules']) > 0) {
            $rules = json_encode($collection['rules']);
            $collectionModel->rules = $rules;
        } else {
            $collectionModel->rules = null;
        }
        $collectionModel->save();
    }

    /**
     * Sync Meta Fields
     * @param $store
     * @param null $collection_id
     */
    public static function syncMetaFields($store, $collection_id = null)
    {
        $store_id = strval(trim($store->store_id));
        if (is_null($collection_id)) {
            $model = Collection::whereStoreId($store_id)->get();
        } else {
            $model = Collection::whereStoreId($store_id)->whereCollectionId($collection_id)->get();
        }
        foreach ($model as $item) {
            $model_id = strval(trim($item['collection_id']));
            $last_page = false;
            $params = array('limit' => 250);
            $deleted_items = array();
            while (!$last_page) {
                $end_point = "/admin/api/" . env('PUBLIC_APP_API_VERSION') . "/collections/" . $model_id . "/metafields.json";
                $request = Shopify::call($store->token, $store->domain, $end_point, $params, 'GET');
                $header = AppHelper::getShopifyNextPageArray($request['headers']);
                $response = json_decode($request['response'], JSON_PRETTY_PRINT);
                if (isset($response['metafields']) && count($response['metafields']) > 0) {
                    foreach ($response['metafields'] as $metafield) {
                        $meta_id = strval(trim($metafield['id']));
                        $owner_id = strval(trim($metafield['owner_id']));
                        Metafield::updateOrCreate([
                            'store_id' => $store_id,
                            'meta_id' => $meta_id,
                            'meta_owner_id' => $owner_id
                        ], [
                            'store_id' => $store_id,
                            'meta_id' => $meta_id,
                            'meta_owner_id' => $owner_id,
                            'meta_owner_resource' => $metafield['owner_resource'],
                            "meta_namespace" => $metafield['namespace'],
                            'meta_key' => $metafield['key'],
                            'meta_value' => $metafield['value'],
                            'meta_value_type' => $metafield['value_type'],
                            'meta_description' => $metafield['description'],
                            'meta_created_at' => $metafield['created_at'],
                            'meta_updated_at' => $metafield['updated_at']
                        ]);
                        array_push($deleted_items, $meta_id);
                    }
                }
                if (isset($header['next_page'])) {
                    $params['page_info'] = $header['next_page'];
                }
                $last_page = $header['last_page'];
            }
            if ($collection_id) {
                Metafield::whereNotIn('meta_id', $deleted_items)
                    ->where('meta_owner_id', $collection_id)
                    ->where('meta_owner_resource', 'collection')
                    ->whereStoreId($store_id)
                    ->delete();
            }
        }
    }
}
