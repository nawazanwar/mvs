<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Metafield extends Model
{
    use HasFactory;

    protected $table = 'metafields';
    protected $fillable = [
        'store_id',
        'meta_id',
        'meta_owner_id',
        'meta_owner_resource',
        "meta_namespace",
        'meta_key',
        'meta_value',
        'meta_value_type',
        'meta_description',
        'meta_created_at',
        'meta_updated_at'
    ];

    /**
     * @return BelongsTo
     */
    public function store(): BelongsTo
    {
        return $this->belongsTo(Store::class);
    }
}

