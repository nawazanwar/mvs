<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class OrderBilling extends Model
{
    use HasFactory;

    protected $table = 'order_billings';
    protected $fillable = [
        'store_id',
        'order_id',
        'billing_name',
        'billing_phone',
        'billing_company',
        'billing_address1',
        'billing_address2',
        'billing_city',
        'billing_province',
        'billing_province_code',
        'billing_country',
        'billing_country_code',
        'billing_zip',
        'billing_latitude',
        'billing_longitude'
    ];

    /**
     * @return BelongsTo
     */
    public function store(): BelongsTo
    {
        return $this->belongsTo(Store::class);
    }
}
