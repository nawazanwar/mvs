<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PrivacyPolicy extends Model
{
    use HasFactory;

    protected $table = 'privacy_policies';
    protected $fillable = [
        'store_id', 'email', 'term_of_use', 'privacy_policy'
    ];
}
