<?php

namespace App\Models;

use App\AppHelper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $table = 'orders';
    protected $fillable = [
        'store_id',
        'order_id',
        'order_number',
        'total_price',
        'subtotal_price',
        'total_price_usd',
        'total_weight',
        'total_tax',
        'tax_lines',
        'currency',
        'financial_status',
        'fulfillment_status',
        'total_discounts',
        'discount_applications',
        'discount_codes',
        'total_line_items_price',
        'note',
        'note_attributes',
        'client_details',
        'payment_details',
        'payment_gateway_names',
        'processing_method',
        'checkout_id',
        'source_name',
        'contact_email',
        'reference',
        'confirmed',
        'cancel_reason',
        'order_created_at',
        'order_updated_at',
        'order_cancelled_at',
        'order_closed_at',
        'order_processed_at'
    ];

    /**
     * Manage Orders
     * @param $order
     * @param $store
     * @return array
     */
    public static function manageOrder($order, $store): array
    {
        $finalArray = array();
        $store_id = strval(trim($store->store_id));
        $order_id = strval(trim($order['id']));
        $orderData = [
            'store_id' => $store_id,
            'order_id' => $order_id,
            'order_number' => $order['order_number'],
            'subtotal_price' => $order['subtotal_price'],
            'total_price' => $order['total_price'],
            'total_price_usd' => $order['total_price_usd'],
            'total_weight' => $order['total_weight'],
            'total_tax' => $order['total_tax'],
            'tax_lines' => isset($order['tax_lines']) ? json_encode($order['tax_lines']) : null,
            'currency' => $order['currency'],
            'financial_status' => $order['financial_status'],
            'fulfillment_status' => $order['fulfillment_status'],
            'total_discounts' => $order['total_discounts'],
            'discount_applications' => isset($order['discount_applications']) ? json_encode($order['discount_applications']) : null,
            'discount_codes' => isset($order['discount_codes']) ? json_encode($order['discount_codes']) : null,
            'total_line_items_price' => $order['total_line_items_price'],
            'note' => $order['note'],
            'note_attributes' => isset($order['note_attributes']) ? json_encode($order['note_attributes']) : null,
            'client_details' => isset($order['client_details']) ? json_encode($order['client_details']) : null,
            'payment_gateway_names' => isset($order['payment_gateway_names']) ? json_encode($order['payment_gateway_names'], JSON_FORCE_OBJECT) : null,
            'processing_method' => isset($order['processing_method']) ? $order['processing_method'] : null,
            'checkout_id' => strval(trim($order['checkout_id'])),
            'source_name' => $order['source_name'],
            'contact_email' => $order['contact_email'],
            'reference' => $order['reference'],
            'confirmed' => $order['confirmed'],
            'cancel_reason' => $order['cancel_reason'],
            'order_created_at' => $order['created_at'],
            'order_updated_at' => $order['updated_at'],
            'order_cancelled_at' => $order['cancelled_at'],
            'order_closed_at' => $order['closed_at'],
            'order_processed_at' => $order['processed_at']
        ];
        Order::updateOrCreate([
            'store_id' => $store_id,
            'order_id' => $order_id
        ], $orderData);
        if (isset($order['line_items']) && count($order['line_items']) > 0) {
            foreach ($order['line_items'] as $line_item) {
                $lineItemId = strval(trim($line_item['id']));
                $lineItemProductId = strval(trim($line_item['product_id']));
                $lineItemVariantId = strval(trim($line_item['variant_id']));
                $orderLineItemData = [
                    'store_id' => $store_id,
                    'order_id' => $order_id,
                    'line_item_id' => $lineItemId,
                    'line_item_product_id' => $lineItemProductId,
                    'line_item_variant_id' => $lineItemVariantId,
                    'line_item_name' => $line_item['name'],
                    'line_item_quantity' => $line_item['quantity'],
                    'line_item_sku' => $line_item['sku'],
                    'line_item_vendor' => $line_item['vendor'],
                    'line_item_fulfillment_service' => $line_item['fulfillment_service'],
                    'line_item_requires_shipping' => $line_item['requires_shipping'],
                    'line_item_taxable' => $line_item['taxable'],
                    'line_item_grams' => $line_item['grams'],
                    'line_item_price' => $line_item['price'],
                    'line_item_total_discount' => $line_item['total_discount'],
                    'line_item_fulfillment_status' => $line_item['fulfillment_status']
                ];
                OrderItem::updateOrCreate([
                    'store_id' => $store_id,
                    'order_id' => $order_id,
                    'line_item_id' => $lineItemId,
                    'line_item_product_id' => $lineItemProductId,
                    'line_item_variant_id' => $lineItemVariantId
                ], $orderLineItemData);

                if (AppHelper::isVendorProduct($lineItemProductId, $store_id)) {

                    $vendor_store_id = AppHelper::getVendorStoreId($lineItemProductId, $store_id);

                    $finalArray[$vendor_store_id]['order']['id'] = $order_id;
                    $finalArray[$vendor_store_id]['order']['order_number'] = $order['order_number'];
                    $finalArray[$vendor_store_id]['order']['email'] = $order['contact_email'];
                    $finalArray[$vendor_store_id]['order']['note'] = $order['note'];
                    $finalArray[$vendor_store_id]['order']['financial_status'] = $order['financial_status'];
                    $lineItemVariantId = strval(trim($line_item['variant_id']));
                    $finalArray[$vendor_store_id]['order']['line_items'][] = [
                        'variant_id' => AppHelper::getVendorVariantId($lineItemProductId, $lineItemVariantId, $store_id),
                        'quantity' => $line_item['quantity'],
                    ];

                    if (isset($order['billing_address']) && count($order['billing_address']) > 0) {
                        $finalArray[$vendor_store_id]['order']['billing_address'] = $order['billing_address'];
                    }
                    if (isset($order['shipping_address']) && count($order['shipping_address']) > 0) {
                        $finalArray[$vendor_store_id]['order']['shipping_address'] = $order['shipping_address'];
                    }
                }
            }
        }
        if (isset($order['customer']) && count($order['customer']) > 0) {
            $customer = $order['customer'];
            $orderCustomerData = [
                'store_id' => $store_id,
                'order_id' => $order_id,
                'customer_id' => strval(trim($customer['id'])),
                'customer_first_name' => $customer['first_name'],
                'customer_last_name' => $customer['last_name'],
                'customer_email' => $customer['email'],
                'customer_phone' => $customer['phone'],
                'customer_tags' => $customer['tags'],
                'customer_orders_count' => $customer['orders_count'],
                'customer_state' => $customer['state'],
                'customer_total_spent' => $customer['total_spent'],
                'customer_company' => $customer['default_address']['company'],
                'customer_address1' => $customer['default_address']['address1'],
                'customer_address2' => $customer['default_address']['address2'],
                'customer_city' => $customer['default_address']['city'],
                'customer_province' => $customer['default_address']['province'],
                'customer_country' => $customer['default_address']['country'],
                'customer_zip' => $customer['default_address']['zip']
            ];
            OrderCustomer::updateOrCreate([
                'store_id' => $store_id,
                'order_id' => $order_id,
                'customer_id' => $orderCustomerData['customer_id']
            ], $orderCustomerData);
        }
        if (isset($order['billing_address']) && count($order['billing_address']) > 0) {
            $orderBillingAddress = $order['billing_address'];
            $billingAddressData = [
                'store_id' => $store_id,
                'order_id' => $order_id,
                'billing_name' => $orderBillingAddress['name'],
                'billing_phone' => $orderBillingAddress['phone'],
                'billing_company' => $orderBillingAddress['company'],
                'billing_address1' => $orderBillingAddress['address1'],
                'billing_address2' => $orderBillingAddress['address2'],
                'billing_city' => $orderBillingAddress['city'],
                'billing_province' => $orderBillingAddress['province'],
                'billing_province_code' => $orderBillingAddress['province_code'],
                'billing_country' => $orderBillingAddress['country'],
                'billing_country_code' => $orderBillingAddress['country_code'],
                'billing_zip' => $orderBillingAddress['zip'],
                'billing_latitude' => $orderBillingAddress['latitude'],
                'billing_longitude' => $orderBillingAddress['longitude']
            ];
            OrderBilling::updateOrCreate([
                'store_id' => $store_id,
                'order_id' => $order_id
            ], $billingAddressData);
        }
        if (isset($order['shipping_address']) && count($order['shipping_address']) > 0) {
            $orderShippingAddress = $order['shipping_address'];
            $shippingAddressData = [
                'store_id' => $store_id,
                'order_id' => $order_id,
                'shipping_name' => $orderShippingAddress['name'],
                'shipping_phone' => $orderShippingAddress['phone'],
                'shipping_company' => $orderShippingAddress['company'],
                'shipping_address1' => $orderShippingAddress['address1'],
                'shipping_address2' => $orderShippingAddress['address2'],
                'shipping_city' => $orderShippingAddress['city'],
                'shipping_province' => $orderShippingAddress['province'],
                'shipping_province_code' => $orderShippingAddress['province_code'],
                'shipping_country' => $orderShippingAddress['country'],
                'shipping_country_code' => $orderShippingAddress['country_code'],
                'shipping_zip' => $orderShippingAddress['zip'],
                'shipping_latitude' => $orderShippingAddress['latitude'],
                'shipping_longitude' => $orderShippingAddress['longitude']
            ];
            OrderShipping::updateOrCreate([
                'store_id' => $store_id,
                'order_id' => $order_id
            ], $shippingAddressData);
        }
        return $finalArray;
    }

    /**
     * Sync Customer Meta fields
     * @param $store
     * @param null $order_id
     */
    public static function syncMetaFields($store, $order_id = null)
    {
        $store_id = strval(trim($store->store_id));
        if (is_null($order_id)) {
            $model = Order::whereStoreId($store_id)->get();
        } else {
            $model = Order::whereStoreId($store_id)->whereOrderId($order_id)->get();
        }
        foreach ($model as $item) {
            $model_id = strval(trim($item['order_id']));
            $last_page = false;
            $params = array('limit' => 250);
            $deleted_items = array();
            while (!$last_page) {
                $end_point = "/admin/api/" . env('PUBLIC_APP_API_VERSION') . "/orders/" . $model_id . "/metafields.json";
                $request = Shopify::call($store->token, $store->domain, $end_point, $params, 'GET');
                $header = AppHelper::getShopifyNextPageArray($request['headers']);
                $response = json_decode($request['response'], JSON_PRETTY_PRINT);
                if (isset($response['metafields']) && count($response['metafields']) > 0) {
                    foreach ($response['metafields'] as $metafield) {
                        $meta_id = strval(trim($metafield['id']));
                        $owner_id = strval(trim($metafield['owner_id']));
                        $metaObject = Metafield::updateOrCreate([
                            'store_id' => $store_id,
                            'meta_id' => $meta_id,
                            'meta_owner_id' => $owner_id
                        ], [
                            'store_id' => $store_id,
                            'meta_id' => $meta_id,
                            'meta_owner_id' => $owner_id,
                            'meta_owner_resource' => $metafield['owner_resource'],
                            "meta_namespace" => $metafield['namespace'],
                            'meta_key' => $metafield['key'],
                            'meta_value' => $metafield['value'],
                            'meta_value_type' => $metafield['value_type'],
                            'meta_description' => $metafield['description'],
                            'meta_created_at' => $metafield['created_at'],
                            'meta_updated_at' => $metafield['updated_at']
                        ]);
                        if (strval($metaObject->meta_key) == 'seller_store_id') {
                            $item->seller_store_id = strval($metaObject->meta_value);
                            $item->save();
                        }
                        if (strval($metaObject->meta_key) == 'seller_order_id') {
                            $item->seller_order_id = strval($metaObject->meta_value);
                            $item->save();
                        }
                        if (strval($metaObject->meta_key) == 'seller_order_number') {
                            $item->seller_order_number = strval($metaObject->meta_value);
                            $item->save();
                        }
                        array_push($deleted_items, $meta_id);
                    }
                }
                if (isset($header['next_page'])) {
                    $params['page_info'] = $header['next_page'];
                }
                $last_page = $header['last_page'];
            }
            if ($order_id) {
                Metafield::whereNotIn('meta_id', $deleted_items)
                    ->where('meta_owner_id', $order_id)
                    ->whereStoreId($store_id)
                    ->delete();
            }
        }
    }
}
