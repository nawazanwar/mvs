<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class OrderItem extends Model
{
    use HasFactory;

    protected $table = 'order_items';
    protected $fillable = [
        'store_id',
        'order_id',
        'line_item_id',
        'line_item_product_id',
        'line_item_variant_id',
        'line_item_name',
        'line_item_quantity',
        'line_item_sku',
        'line_item_vendor',
        'line_item_fulfillment_service',
        'line_item_requires_shipping',
        'line_item_taxable',
        'line_item_grams',
        'line_item_price',
        'line_item_total_discount',
        'line_item_fulfillment_status',
        'line_item_fulfillable_quantity'
    ];

    /**
     * @return BelongsTo
     */
    public function store(): BelongsTo
    {
        return $this->belongsTo(Store::class);
    }
}
