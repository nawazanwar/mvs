<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class FulfilmentLineItem extends Model
{
    use HasFactory;

    protected $table = 'fulfilment_line_items';
    protected $fillable = [
        'store_id',
        'fulfillment_id',
        'line_item_id',
        'line_item_product_id',
        'line_item_variant_id',
        'line_item_name',
        'line_item_quantity',
        'line_item_sku',
        'line_item_vendor',
        'line_item_fulfillment_service',
        'line_item_requires_shipping',
        'line_item_taxable',
        'line_item_grams',
        'line_item_price',
        'line_item_total_discount',
        'line_item_fulfillment_status',
        'line_item_fulfillable_quantity',
        'line_item_product_exists'
    ];
}
