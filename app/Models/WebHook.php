<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class WebHook extends Model
{

    protected $table = 'web_hooks';
    protected $fillable = [
        'store_id', 'name', 'web_hook_id', 'address'
    ];

    /**
     * Create Customer - WebHook
     * @param $store
     */
    public static function createCustomer($store)
    {
        $data = array(
            'webhook' =>
                array(
                    'topic' => 'customers/create',
                    'address' => env('PUBLIC_APP_ABSOLUTE_URL') . "webhooks/customers/create",
                    'format' => 'json'
                )
        );
        self::call($store, $data, 'customers/create');
    }

    /**
     * Create Collection - WebHook
     * @param $store
     */
    public static function createCollection($store)
    {
        $data = array(
            'webhook' =>
                array(
                    'topic' => 'collections/create',
                    'address' => env('PUBLIC_APP_ABSOLUTE_URL') . "webhooks/collections/create",
                    'format' => 'json'
                )
        );
        self::call($store, $data, 'collections/create');
    }

    /**
     * Create Product - WebHook
     * @param $store
     */
    public static function createProduct($store)
    {
        $data = array(
            'webhook' =>
                array(
                    'topic' => 'products/create',
                    'address' => env('PUBLIC_APP_ABSOLUTE_URL') . "webhooks/products/create",
                    'format' => 'json'
                )
        );
        self::call($store, $data, 'products/create');
    }

    /**
     * Create Order - WebHook
     * @param $store
     */
    public static function createOrder($store)
    {
        $data = array(
            'webhook' =>
                array(
                    'topic' => 'orders/create',
                    'address' => env('PUBLIC_APP_ABSOLUTE_URL') . "webhooks/orders/create",
                    'format' => 'json'
                )
        );
        self::call($store, $data, 'orders/create');
    }

    /**
     * Update Customer - WebHook
     * @param $store
     */
    public static function updateCustomer($store)
    {
        $data = array(
            'webhook' =>
                array(
                    'topic' => 'customers/update',
                    'address' => env('PUBLIC_APP_ABSOLUTE_URL') . "webhooks/customers/update",
                    'format' => 'json'
                )
        );
        self::call($store, $data, 'customers/update');
    }

    /**
     * Update Collection - WebHook
     * @param $store
     */
    public static function updateCollection($store)
    {
        $data = array(
            'webhook' =>
                array(
                    'topic' => 'collections/update',
                    'address' => env('PUBLIC_APP_ABSOLUTE_URL') . "webhooks/collections/update",
                    'format' => 'json'
                )
        );
        self::call($store, $data, 'collections/update');
    }

    /**
     * Update Product - WebHook
     * @param $store
     */
    public static function updateProduct($store)
    {
        $data = array(
            'webhook' =>
                array(
                    'topic' => 'products/update',
                    'address' => env('PUBLIC_APP_ABSOLUTE_URL') . "webhooks/products/update",
                    'format' => 'json'
                )
        );
        self::call($store, $data, 'products/update');
    }

    /**
     * Update Order - WebHook
     * @param $store
     */
    public static function updateOrder($store)
    {
        $data = array(
            'webhook' =>
                array(
                    'topic' => 'orders/updated',
                    'address' => env('PUBLIC_APP_ABSOLUTE_URL') . "webhooks/orders/update",
                    'format' => 'json'
                )
        );
        self::call($store, $data, 'orders/updated');
    }

    /**
     * Delete Collection - WebHook
     * @param $store
     */
    public static function deleteCollection($store)
    {
        $data = array(
            'webhook' =>
                array(
                    'topic' => 'collections/delete',
                    'address' => env('PUBLIC_APP_ABSOLUTE_URL') . "webhooks/collections/delete",
                    'format' => 'json'
                )
        );
        self::call($store, $data, 'collections/delete');
    }

    /**
     * Delete Product - WebHook
     * @param $store
     */
    public static function deleteProduct($store)
    {
        $data = array(
            'webhook' =>
                array(
                    'topic' => 'products/delete',
                    'address' => env('PUBLIC_APP_ABSOLUTE_URL') . "webhooks/products/delete",
                    'format' => 'json'
                )
        );
        self::call($store, $data, 'products/delete');
    }

    /**
     * Delete Order - WebHook
     * @param $store
     */
    public static function deleteOrder($store)
    {
        $data = array(
            'webhook' =>
                array(
                    'topic' => 'orders/delete',
                    'address' => env('PUBLIC_APP_ABSOLUTE_URL') . "webhooks/orders/delete",
                    'format' => 'json'
                )
        );
        self::call($store, $data, 'orders/delete');
    }

    /**
     * Delete Customer - WebHook
     * @param $store
     */
    public static function deleteCustomer($store)
    {
        $data = array(
            'webhook' =>
                array(
                    'topic' => 'customers/delete',
                    'address' => env('PUBLIC_APP_ABSOLUTE_URL') . "webhooks/customers/delete",
                    'format' => 'json'
                )
        );
        self::call($store, $data, 'customers/delete');
    }

    /**
     * Delete Customer - WebHook
     * @param $store
     */
    public static function createFulfillment($store)
    {
        $data = array(
            'webhook' =>
                array(
                    'topic' => 'fulfillments/create',
                    'address' => env('PUBLIC_APP_ABSOLUTE_URL') . "webhooks/fulfillment/create",
                    'format' => 'json'
                )
        );
        self::call($store, $data, 'fulfillments/create');
    }

    /**
     * Delete Customer - WebHook
     * @param $store
     */
    public static function updateFulfillment($store)
    {
        $data = array(
            'webhook' =>
                array(
                    'topic' => 'fulfillments/update',
                    'address' => env('PUBLIC_APP_ABSOLUTE_URL') . "webhooks/fulfillment/update",
                    'format' => 'json'
                )
        );
        self::call($store, $data, 'fulfillments/update');
    }

    /**
     * @param $store
     * @param $data
     * @param $name
     */
    public static function call($store, $data, $name)
    {
        $end_point = "/admin/api/" . env('PUBLIC_APP_API_VERSION') . "/webhooks.json";
        $response = Shopify::call($store->token, $store->domain, $end_point, $data, 'POST');
        $response = json_decode($response['response'], JSON_PRETTY_PRINT);
        if (isset($response['webhook'])) {
            $webHookModel = new WebHook();
            $webHookModel->name = $name;
            $webHookModel->web_hook_id = strval($response['webhook']['id']);
            $webHookModel->store_id = strval(trim($store->store_id));
            $webHookModel->address = $response['webhook']['address'];
            $webHookModel->save();
        }
    }
}
