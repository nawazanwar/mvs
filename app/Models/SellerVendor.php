<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SellerVendor extends Model
{
    use HasFactory;
    use HasFactory;

    protected $table = 'seller_vendors';
    /**
     * @var array
     */
    protected $fillable = [
        'seller_store_id', 'vendor_store_id'
    ];
    protected $dates = [
        'created_at', 'updated_at'
    ];
}
