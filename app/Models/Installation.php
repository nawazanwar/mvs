<?php

namespace App\Models;

use App\AppHelper;
use App\Models\Shopify;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Installation extends Model
{
    use HasFactory;

    private static $collections = array();

    public static function getCollections($store): array
    {
        self::getCustomCollection($store);
        self::getSmartCollection($store);
        return self::$collections;
    }

    public static function getCustomCollection($store)
    {
        $last_page = false;
        $params = array('limit' => 250);
        while (!$last_page) {
            $end_point = "/admin/api/" . env('PUBLIC_APP_API_VERSION') . "/custom_collections.json";
            $request = Shopify::call($store->token, $store->domain, $end_point, $params, 'GET');
            $header = AppHelper::getShopifyNextPageArray($request['headers']);
            $response = json_decode($request['response'], JSON_PRETTY_PRINT);
            if (isset($response['custom_collections']) && count($response['custom_collections']) > 0) {
                foreach ($response['custom_collections'] as $custom_collection) {
                    $collection_id = strval(trim($custom_collection['id']));
                    $collection_title = strval($custom_collection['title']);
                    self::$collections[$collection_id] = $collection_title;
                }
            }
            if (isset($header['next_page'])) {
                $params['page_info'] = $header['next_page'];
            }
            $last_page = $header['last_page'];
        }
    }

    public static function getSmartCollection($store)
    {
        $last_page = false;
        $params = array('limit' => 250);
        while (!$last_page) {
            $end_point = "/admin/api/" . env('PUBLIC_APP_API_VERSION') . "/smart_collections.json";
            $request = Shopify::call($store->token, $store->domain, $end_point, $params, 'GET');
            $header = AppHelper::getShopifyNextPageArray($request['headers']);
            $response = json_decode($request['response'], JSON_PRETTY_PRINT);
            if (isset($response['smart_collections']) && count($response['smart_collections']) > 0) {
                foreach ($response['smart_collections'] as $smart_collection) {
                    $collection_id = strval(trim($smart_collection['id']));
                    $collection_title = strval($smart_collection['title']);
                    self::$collections[$collection_id] = $collection_title;
                }
            }
            if (isset($header['next_page'])) {
                $params['page_info'] = $header['next_page'];
            }
            $last_page = $header['last_page'];
        }
    }
}
