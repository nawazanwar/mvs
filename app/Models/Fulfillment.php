<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\DB;

class Fulfillment extends Model
{
    use HasFactory;

    protected $table = 'fulfillment';
    protected $fillable = [
        'store_id',
        'order_id',
        'fulfillment_id',
        'location_id',
        'status',
        'shipment_status',
        'service',
        'tracking_company',
        'fulfilment_created_at',
        'fulfilment_updated_at'
    ];

    public static function manage($fulfilment, $store)
    {
        $finalArray = array();
        $store_id = strval(trim($store->store_id));
        $order_id = strval(trim($fulfilment['order_id'])); // Vendor order ID
        $fulfillment_id = strval(trim($fulfilment['id']));
        $location_id = strval(trim($fulfilment['location_id']));
        $status = strval(trim($fulfilment['status']));
        $shipment_status = strval(trim($fulfilment['shipment_status']));
        $service = strval(trim($fulfilment['service']));
        $tracking_company = strval(trim($fulfilment['tracking_company']));
        $fulfilment_created_at = strval(trim($fulfilment['created_at']));
        $fulfilment_updated_at = strval(trim($fulfilment['updated_at']));

        $finalArray['fulfillment']['location_id'] = $location_id;
        $finalArray['fulfillment']['tracking_number'] = $fulfilment['tracking_number'];

        Fulfillment::updateOrCreate([
            'store_id' => $store_id,
            'order_id' => $order_id,
            'fulfillment_id' => $fulfillment_id
        ], [
            'store_id' => $store_id,
            'order_id' => $order_id,
            'fulfillment_id' => $fulfillment_id,
            'location_id' => $location_id,
            'status' => $status,
            'shipment_status' => $shipment_status,
            'service' => $service,
            'tracking_company' => $tracking_company,
            'fulfilment_created_at' => $fulfilment_created_at,
            'fulfilment_updated_at' => $fulfilment_updated_at
        ]);
        if (isset($fulfilment['line_items']) && count($fulfilment['line_items']) > 0) {
            foreach ($fulfilment['line_items'] as $line_item) {
                $line_item_id = strval(trim($line_item['id']));
                $line_item_product_id = strval(trim($line_item['product_id']));
                $line_item_variant_id = strval(trim($line_item['variant_id']));
                $finalArray['fulfillment']['line_items'][]['id'] = $line_item_variant_id;

                FulfilmentLineItem::updateOrCreate([
                    "store_id" => $store_id,
                    "fulfillment_id" => $fulfillment_id,
                    'line_item_id' => $line_item_id,
                    "line_item_product_id" => $line_item_product_id,
                    "line_item_variant_id" => $line_item_variant_id,
                ], [
                    "store_id" => $store_id,
                    "fulfillment_id" => $fulfillment_id,
                    'line_item_id' => $line_item_id,
                    "line_item_product_id" => $line_item_product_id,
                    "line_item_variant_id" => $line_item_variant_id,
                    "line_item_name" => $line_item['title'],
                    "line_item_quantity" => $line_item['quantity'],
                    "line_item_sku" => $line_item['sku'],
                    "line_item_vendor" => $line_item['vendor'],
                    "line_item_fulfillment_service",
                    "line_item_requires_shipping",
                    "line_item_taxable",
                    "line_item_grams",
                    "line_item_price",
                    "line_item_total_discount",
                    "line_item_fulfillment_status",
                    "line_item_fulfillable_quantity",
                    "line_item_product_exists"
                ]);
            }
        }
        self::addSellerFulfillment($fulfilment);
    }

    /**
     * @param $fulfilment
     * @param $store
     * @return false
     */
    public static function addSellerFulfillment($fulfilment)
    {
        $order_id = strval(trim($fulfilment['order_id'])); // Vendor order ID
        $vendor_order = Order::whereOrderId($order_id)->first();
        $seller_order_id = $vendor_order->seller_order_id;
        $seller_store = Store::whereStoreId($vendor_order->seller_store_id)->first();

        file_put_contents('fulfilment_seller_store.txt', print_r($seller_store, true));

        if (intval($seller_order_id) == 0) {
            return false;
        }

        $seller_fulfill = [
            'location_id' => $seller_store->primary_location_id,
            'tracking_number' => $fulfilment['tracking_number'],
            'tracking_url' => $fulfilment['tracking_url'],
            'tracking_company' => $fulfilment['tracking_company']
        ];
        foreach ($fulfilment['line_items'] as $line_item) {
            $line_item_variant_id = strval(trim($line_item['variant_id']));
            $query = "select
                           oi.line_item_id as vendor_item_id,
                           oi2.line_item_id as seller_item_id
                        FROM order_items oi
                            LEFT JOIN variants v ON oi.line_item_variant_id = v.vendor_variant_id
                            LEFT JOIN order_items oi2 ON v.variant_id = oi2.line_item_variant_id
                        WHERE oi.order_id = '{$order_id}'
                        AND
                              oi.line_item_variant_id ='{$line_item_variant_id}'
                        AND
                              oi2.order_id = '{$seller_order_id}'";
            file_put_contents('fulfilment_query.txt', $query);
            $sellerItem = DB::select(DB::raw($query));
            file_put_contents('fulfilment_query_data.txt', print_r($sellerItem, true));
            if (isset($sellerItem[0])) {
                $seller_fulfill['line_items'][] = [
                    'id' => $sellerItem[0]->seller_item_id,
                    'quantity' => $line_item['quantity']
                ];
            }
        }
        $params = [
            'fulfillment' => $seller_fulfill
        ];
        file_put_contents('fulfilment_params.txt', print_r($params, true));
        $end_point = "/admin/api/" . env('PUBLIC_APP_API_VERSION') . "/orders/" . $seller_order_id . "/fulfillments.json";
        file_put_contents('fulfilment_end_point.txt', $end_point);
        $request = Shopify::call($seller_store->token, $seller_store->domain, $end_point, $params, 'POST');
        file_put_contents('fulfilment_request.txt', print_r($request, true));
        // Do api call seller order fulfillment
    }
}
