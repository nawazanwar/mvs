<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class OrderShipping extends Model
{
    use HasFactory;
    protected $table = 'order_shippings';
    protected $fillable = [
        'store_id',
        'order_id',
        'shipping_name',
        'shipping_phone',
        'shipping_company',
        'shipping_address1',
        'shipping_address2',
        'shipping_city',
        'shipping_province',
        'shipping_province_code',
        'shipping_country',
        'shipping_country_code',
        'shipping_zip',
        'shipping_latitude',
        'shipping_longitude'
    ];

    /**
     * @return BelongsTo
     */
    public function store(): BelongsTo
    {
        return $this->belongsTo(Store::class);
    }
}
