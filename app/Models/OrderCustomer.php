<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class OrderCustomer extends Model
{
    use HasFactory;

    protected $table = 'order_customers';
    protected $fillable = [
        'store_id',
        'order_id',
        'customer_id',
        'customer_first_name',
        'customer_last_name',
        'customer_email',
        'customer_phone',
        'customer_tags',
        'customer_orders_count',
        'customer_state',
        'customer_total_spent',
        'customer_company',
        'customer_address1',
        'customer_address2',
        'customer_city',
        'customer_province',
        'customer_country',
        'customer_zip'
    ];

    /**
     * @return BelongsTo
     */
    public function store(): BelongsTo
    {
        return $this->belongsTo(Store::class);
    }
}
