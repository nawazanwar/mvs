<?php

namespace App\Models;

use App\AppHelper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Customer extends Model
{
    use HasFactory;

    protected $table = 'customers';
    protected $fillable = [
        'store_id',
        'customer_id',
        'first_name',
        'last_name',
        'email',
        'verified_email',
        'phone',
        'tags',
        'orders_count',
        'last_order_name',
        'last_order_id',
        'state',
        'total_spent',
        'note',
        'company',
        'address1',
        'address2',
        'city',
        'province',
        'country',
        'zip',
        'addresses'
    ];

    /**
     * Sync Customers from Store
     * @param $store
     */
    public static function sync($store)
    {
        $last_page = false;
        $params = array('limit' => 250);
        while (!$last_page) {
            $end_point = "/admin/api/" . env('PUBLIC_APP_API_VERSION') . "/customers.json";
            $request = Shopify::call($store->token, $store->domain, $end_point, $params, 'GET');
            $header = AppHelper::getShopifyNextPageArray($request['headers']);
            $response = json_decode($request['response'], JSON_PRETTY_PRINT);
            if (isset($response['customers']) && count($response['customers']) > 0) {
                foreach ($response['customers'] as $customer) {
                    self::manageCustomer($customer, $store);
                }
            }
            if (isset($header['next_page'])) {
                $params['page_info'] = $header['next_page'];
            }
            $last_page = $header['last_page'];
        }
    }

    /**
     * Save Customer
     * @param $customer
     * @param $store
     */
    public static function manageCustomer($customer, $store)
    {
        $store_id = strval(trim($store->store_id));
        $customer_id = strval(trim($customer['id']));
        Customer::updateOrCreate([
            'store_id' => $store_id,
            'customer_id' => $customer_id
        ], [
            'store_id' => $store_id,
            'customer_id' => $customer_id,
            'first_name' => isset($customer['first_name']) ? $customer['first_name'] : null,
            'last_name' => isset($customer['last_name']) ? $customer['last_name'] : null,
            'note' => isset($customer['note']) ? $customer['note'] : null,
            'email' => isset($customer['email']) ? $customer['email'] : null,
            'verified_email' => isset($customer['verified_email']) ? $customer['verified_email'] : null,
            'phone' => isset($customer['phone']) ? $customer['phone'] : null,
            'tags' => isset($customer['tags']) ? $customer['tags'] : null,
            'orders_count' => isset($customer['orders_count']) ? $customer['orders_count'] : null,
            'last_order_name' => isset($customer['last_order_name']) ? $customer['last_order_name'] : null,
            'last_order_id' => isset($customer['last_order_id']) ? strval(trim($customer['last_order_id'])) : null,
            'state' => isset($customer['state']) ? $customer['state'] : null,
            'total_spent' => isset($customer['total_spent']) ? $customer['total_spent'] : null,
            'company' => isset($customer['default_address']['company']) ? $customer['default_address']['company'] : null,
            'address1' => isset($customer['default_address']['address1']) ? $customer['default_address']['address1'] : null,
            'address2' => isset($customer['default_address']['address2']) ? $customer['default_address']['address2'] : null,
            'city' => isset($customer['default_address']['city']) ? $customer['default_address']['city'] : null,
            'province' => isset($customer['default_address']['province']) ? $customer['default_address']['province'] : null,
            'country' => isset($customer['default_address']['country']) ? $customer['default_address']['country'] : null,
            'zip' => isset($customer['default_address']['zip']) ? $customer['default_address']['zip'] : null,
            'addresses' => isset($customer['addresses']) ? json_encode($customer['addresses']) : null
        ]);
    }

    /**
     * Sync Customer Meta fields
     * @param $store
     * @param null $customer_id
     */
    public static function syncMetaFields($store, $customer_id = null)
    {
        $store_id = strval(trim($store->store_id));
        if (is_null($customer_id)) {
            $model = Customer::whereStoreId($store_id)->get();
        } else {
            $model = Customer::whereStoreId($store_id)->whereCustomerId($customer_id)->get();
        }
        foreach ($model as $item) {
            $model_id = strval(trim($item['customer_id']));
            $last_page = false;
            $params = array('limit' => 250);
            $deleted_items = array();
            while (!$last_page) {
                $end_point = "/admin/api/" . env('PUBLIC_APP_API_VERSION') . "/customers/" . $model_id . "/metafields.json";
                $request = Shopify::call($store->token, $store->domain, $end_point, $params, 'GET');
                $header = AppHelper::getShopifyNextPageArray($request['headers']);
                $response = json_decode($request['response'], JSON_PRETTY_PRINT);
                if (isset($response['metafields']) && count($response['metafields']) > 0) {
                    foreach ($response['metafields'] as $metafield) {
                        $meta_id = strval(trim($metafield['id']));
                        $owner_id = strval(trim($metafield['owner_id']));
                        Metafield::updateOrCreate([
                            'store_id' => $store_id,
                            'meta_id' => $meta_id,
                            'meta_owner_id' => $owner_id
                        ], [
                            'store_id' => $store_id,
                            'meta_id' => $meta_id,
                            'meta_owner_id' => $owner_id,
                            'meta_owner_resource' => $metafield['owner_resource'],
                            "meta_namespace" => $metafield['namespace'],
                            'meta_key' => $metafield['key'],
                            'meta_value' => $metafield['value'],
                            'meta_value_type' => $metafield['value_type'],
                            'meta_description' => $metafield['description'],
                            'meta_created_at' => $metafield['created_at'],
                            'meta_updated_at' => $metafield['updated_at']
                        ]);
                        array_push($deleted_items, $meta_id);
                    }
                }
                if (isset($header['next_page'])) {
                    $params['page_info'] = $header['next_page'];
                }
                $last_page = $header['last_page'];
            }
            if ($customer_id) {
                Metafield::whereNotIn('meta_id', $deleted_items)
                    ->where('meta_owner_id', $customer_id)
                    ->whereStoreId($store_id)
                    ->delete();
            }
        }
    }

}
