<?php

namespace App\Models;

use App\AppHelper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Product extends Model
{
    use HasFactory;

    protected $table = 'products';

    /**
     * @var array
     */
    protected $fillable = [
        'store_id',
        'product_id',
        'title',
        'handle',
        'vendor',
        'product_type',
        'tags',
        'body_html',
        'template_suffix',
        'published_scope',
        'image',
        'images',
        'site_url',
        'product_created_at',
        'product_updated_at',
        'product_published_at'
    ];
    protected $dates = [
        'created_at', 'updated_at'
    ];

    /**
     * @return HasMany
     */
    public function variants(): HasMany
    {
        return $this->hasMany(Variant::class, 'product_id');
    }


    /**
     * Sync Products from Store
     * @param $store
     */
    public static function syncToLocalDb($store)
    {
        set_time_limit(0);
        $last_page = false;
        $params = array('limit' => 250);
        while (!$last_page) {
            $end_point = "/admin/api/" . env('PUBLIC_APP_API_VERSION') . "/products.json";
            $request = Shopify::call($store->token, $store->domain, $end_point, $params, 'GET');
            $header = AppHelper::getShopifyNextPageArray($request['headers']);
            $response = json_decode($request['response'], JSON_PRETTY_PRINT);
            if (isset($response['products']) && count($response['products']) > 0) {
                foreach ($response['products'] as $product) {
                    self::manageProduct($product, $store);
                }
            }
            if (isset($header['next_page'])) {
                $params['page_info'] = $header['next_page'];
            }
            $last_page = $header['last_page'];
        }
    }

    /**
     * Save Product
     * @param $product
     * @param $store
     * @return mixed
     */
    public static function manageProduct($product, $store)
    {
        $store_id = strval(trim($store->store_id));
        $product_id = strval(trim($product['id']));
        $productModel = Product::updateOrCreate([
            'store_id' => $store_id,
            'product_id' => $product_id
        ], [
            'store_id' => $store_id,
            'product_id' => $product_id,
            'title' => isset($product['title']) ? $product['title'] : null,
            'handle' => isset($product['handle']) ? $product['handle'] : null,
            'vendor' => isset($product['vendor']) ? $product['vendor'] : null,
            'product_type' => isset($product['product_type']) ? $product['product_type'] : null,
            'tags' => isset($product['tags']) ? $product['tags'] : null,
            'body_html' => isset($product['body_html']) ? $product['body_html'] : null,
            'template_suffix' => isset($product['template_suffix']) ? $product['template_suffix'] : null,
            'published_scope' => isset($product['published_scope']) ? $product['published_scope'] : null,
            'product_created_at' => isset($product['created_at']) ? $product['created_at'] : null,
            'product_updated_at' => isset($product['updated_at']) ? $product['updated_at'] : null,
            'product_published_at' => isset($product['published_at']) ? $product['published_at'] : null,
            'site_url' => "https://" . $store->domain . "/products/" . $product['handle']
        ]);
        if (isset($product['image']) && count($product['image'])) {
            $productModel->image = isset($product['image']['src']) ? $product['image']['src'] : null;
        } else {
            $productModel->image = null;
        }
        if (isset($product['images']) && count($product['images']) > 0) {
            $images = json_encode($product['images']);
            $productModel->images = $images;
        } else {
            $productModel->images = null;
        }
        if (isset($product['options']) && count($product['options']) > 0) {
            $productModel->option1 = isset($product['options'][0]['name']) ? $product['options'][0]['name'] : null;
            $productModel->option2 = isset($product['options'][1]['name']) ? $product['options'][1]['name'] : null;
            $productModel->option3 = isset($product['options'][2]['name']) ? $product['options'][2]['name'] : null;
            $productModel->options = json_encode($product['options']);
        }
        $productModel->save();
        if (isset($product['variants']) && count($product['variants'])) {
            Variant::manageVariants($product['variants'], $store);
        }
    }

    /**
     * Sync Meta Fields
     * @param $store
     * @param null $productId
     */
    public static function syncMetaFields($store, $productId = null)
    {
        file_put_contents('meta_sync_ready.txt', true);
        $store_id = strval(trim($store->store_id));
        if (is_null($productId)) {
            $model = Product::whereStoreId($store_id)->get();
        } else {
            $model = Product::whereStoreId($store_id)->whereProductId($productId)->get();
        }
        file_put_contents('meta_sync_products.txt', print_r($model, true));
        foreach ($model as $item) {
            $modelId = strval(trim($item['product_id']));
            $params = array('limit' => 250);
            $deleted_items = array();
            $end_point = "/admin/api/" . env('PUBLIC_APP_API_VERSION') . "/products/" . $modelId . "/metafields.json";
            $request = Shopify::call($store->token, $store->domain, $end_point, $params, 'GET');
            $response = json_decode($request['response'], JSON_PRETTY_PRINT);
            if (isset($response['metafields']) && count($response['metafields']) > 0) {
                file_put_contents('meta_fields.txt', print_r($response['metafields'], true));
                foreach ($response['metafields'] as $metafield) {
                    $meta_id = strval(trim($metafield['id']));
                    $owner_id = strval(trim($metafield['owner_id']));
                    $metaObject = Metafield::updateOrCreate([
                        'store_id' => $store_id,
                        'meta_id' => $meta_id,
                        'meta_owner_id' => $owner_id
                    ], [
                        'store_id' => $store_id,
                        'meta_id' => $meta_id,
                        'meta_owner_id' => $owner_id,
                        'meta_owner_resource' => $metafield['owner_resource'],
                        "meta_namespace" => $metafield['namespace'],
                        'meta_key' => $metafield['key'],
                        'meta_value' => $metafield['value'],
                        'meta_value_type' => $metafield['value_type'],
                        'meta_description' => $metafield['description'],
                        'meta_created_at' => $metafield['created_at'],
                        'meta_updated_at' => $metafield['updated_at']
                    ]);
                    if (strval($metaObject->meta_key) == 'vendor_store_id') {
                        $item->vendor_store_id = strval($metaObject->meta_value);
                        $item->save();
                    }
                    if (strval($metaObject->meta_key) == 'vendor_product_id') {
                        $item->vendor_product_id = strval($metaObject->meta_value);
                        $item->save();
                    }
                    array_push($deleted_items, $meta_id);
                }
            }
            if ($productId) {
                Metafield::whereNotIn('meta_id', $deleted_items)
                    ->where('meta_owner_id', $productId)
                    ->where('meta_owner_resource', 'product')
                    ->whereStoreId($store_id)
                    ->delete();
            }
        }
    }

    /**
     * Get Product Variants
     * @param $storeId
     * @param $productId
     * @return mixed
     */
    public static function getVariants($storeId, $productId)
    {
        return Variant::whereStoreId($storeId)->whereProductId($productId)->get();
    }

    /**
     * Create New Product in Seller Store Based on Vendor Store
     * @param $product
     * @param $sellerStore
     * @param $vendorStore
     */
    public static function createProductInSellerStore($product, $sellerStore, $vendorStore)
    {
        $vendorStoreId = strval(trim($vendorStore['store_id']));
        $vendorProductId = strval(trim($product['id']));
        if (!Product::whereVendorProductId($vendorProductId)->exists()) {
            $productQuery = [
                'title' => isset($product['title']) ? $product['title'] : null,
                'handle' => isset($product['handle']) ? $product['handle'] : null,
                'vendor' => isset($product['vendor']) ? $product['vendor'] : null,
                'product_type' => isset($product['product_type']) ? $product['product_type'] : null,
                'tags' => isset($product['tags']) ? explode(',', $product['tags']) : null,
                'body_html' => isset($product['body_html']) ? $product['body_html'] : null,
                "published_scope" => "global",
                'status' => 'draft',
                "metafields" => [
                    [
                        'key' => 'vendor_store_id',
                        'value' => $vendorStoreId,
                        "value_type" => "string",
                        "namespace" => "global"
                    ],
                    [
                        'key' => 'vendor_product_id',
                        'value' => $vendorProductId,
                        "value_type" => "string",
                        "namespace" => "global"
                    ]
                ]
            ];
            if (isset($product['options']) && count($product['options']) > 0) {
                $productQuery['options'] = $product['options'];
            }
            $variantArray = array();
            if (isset($product['variants']) && count($product['variants'])) {
                foreach ($product['variants'] as $variant) {
                    $variantArray[] = array(
                        'option1' => isset($variant['option1']) ? $variant['option1'] : null,
                        'option2' => isset($variant['option2']) ? $variant['option2'] : null,
                        'option3' => isset($variant['option3']) ? $variant['option3'] : null,
                        'title' => isset($variant['title']) ? $variant['title'] : null,
                        'sku' => isset($variant['sku']) ? $variant['sku'] : null,
                        'price' => isset($variant['price']) ? $variant['price'] : null,
                        'barcode' => isset($variant['barcode']) ? $variant['barcode'] : null,
                        'inventory_quantity' => isset($variant['inventory_quantity']) ? $variant['inventory_quantity'] : null,
                        'inventory_management' => isset($variant['inventory_management']) ? $variant['inventory_management'] : null,
                        "metafields" => [
                            [
                                'key' => 'vendor_store_id',
                                'value' => $vendorStoreId,
                                "value_type" => "string",
                                "namespace" => "global"
                            ],
                            [
                                'key' => 'vendor_product_id',
                                'value' => $vendorProductId,
                                "value_type" => "string",
                                "namespace" => "global"
                            ],
                            [
                                'key' => 'vendor_variant_id',
                                'value' => strval(trim($variant['id'])),
                                "value_type" => "string",
                                "namespace" => "global"
                            ]
                        ]
                    );
                }
                $productQuery['variants'] = $variantArray;
            }
            if (isset($product['images']) && count($product['images']) > 0) {
                $productQuery['images'] = $product['images'];
            }
            $submitQueryParams = [
                'product' => $productQuery
            ];
            file_put_contents('vendor_Seller_product_query.txt', print_r($submitQueryParams, true));
            $end_point = "/admin/api/" . env('PUBLIC_APP_API_VERSION') . "/products.json";
            Shopify::call($sellerStore->token, $sellerStore->domain, $end_point, $submitQueryParams, 'POST');
        }
    }

    /**
     * Update Product In Seller Store / WebHooks
     * @param $vendorProduct
     * @param $sellerProduct
     * @param $sellerStore
     */
    public static function updateProductInSellerStore($vendorProduct, $sellerProduct, $sellerStore)
    {
        if (isset($vendorProduct['variants']) && count($vendorProduct['variants'])) {
            foreach ($vendorProduct['variants'] as $variant) {
                $sellerProductId = strval(trim($sellerProduct->product_id));
                $sellerInventoryId = Variant::whereProductId($sellerProductId)->value('inventory_item_id');
                $inventoryLevelParams = [
                    'location_id' => $sellerStore->primary_location_id,
                    'inventory_item_id' => $sellerInventoryId,
                    'available' => isset($variant['inventory_quantity']) ? $variant['inventory_quantity'] : null
                ];
                $inventoryLevelEndpoint = "/admin/api/" . env('PUBLIC_APP_API_VERSION') . "/inventory_levels/set.json";
                Shopify::call($sellerStore->token, $sellerStore->domain, $inventoryLevelEndpoint, $inventoryLevelParams, 'POST');
            }
        }
    }
}
