<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CollectionProduct extends Model
{
    use HasFactory;

    protected $table = 'collection_products';
    protected $fillable = [
        'store_id', 'collection_id', 'product_id'
    ];
    protected $dates = [
        'created_at', 'updated_at'
    ];
}
