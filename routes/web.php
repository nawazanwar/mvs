<?php

use App\Http\Controllers\FulFillmentController;
use App\Http\Controllers\VendorController;
use App\Models\VendorSetting;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CollectionController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\SyncController;
use App\Http\Controllers\SettingController;

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/install', [HomeController::class, 'install'])->name('install');
Route::get('/token', [HomeController::class, 'token'])->name('token');

Route::prefix('webhooks')->group(function () {

    Route::post('customers/create', [CustomerController::class, 'webhookCustomerCreate'])->name('webhook-customers-create');
    Route::post('customers/update', [CustomerController::class, 'webhookCustomerUpdate'])->name('webhook-customers-update');
    Route::post('customers/delete', [CustomerController::class, 'webhookCustomerDelete'])->name('webhook-customers-create');

    Route::post('collections/create', [CollectionController::class, 'webhookCollectionCreate'])->name('webhook-collections-create');
    Route::post('collections/update', [CollectionController::class, 'webhookCollectionUpdate'])->name('webhook-collections-update');
    Route::post('collections/delete', [CollectionController::class, 'webhookCollectionDelete'])->name('webhook-collections-create');

    Route::post('products/create', [ProductController::class, 'webhookProductCreate'])->name('webhook-products-create');
    Route::post('products/update', [ProductController::class, 'webhookProductUpdate'])->name('webhook-products-update');
    Route::post('products/delete', [ProductController::class, 'webhookProductDelete'])->name('webhook-products-create');

    Route::post('orders/create', [OrderController::class, 'webhookOrderCreate'])->name('webhook-orders-create');
    Route::post('orders/update', [OrderController::class, 'webhookOrderUpdate'])->name('webhook-orders-update');
    Route::post('orders/delete', [OrderController::class, 'webhookOrderDelete'])->name('webhook-orders-create');

    Route::post('fulfillment/create', [FulFillmentController::class, 'webhookFulFillmentCreate'])->name('webhook-fulfillment-create');
    Route::post('fulfillment/update', [FulFillmentController::class, 'webhookFulFillmentUpdate'])->name('webhook-fulfillment-update');

});

Route::prefix('sync')->group(function () {
    Route::get('vendor/products', [SyncController::class, 'vendorProducts'])->name('sync-vendor-products');
    Route::get('vendor/images', [SyncController::class, 'vendorImages'])->name('sync-vendor-images');
});

Route::post('privacy-policy-settings-store', [SettingController::class, 'storePrivacyPolicy'])->name('privacy-policy-settings-store');
Route::post('vendor-settings-store', [SettingController::class, 'storeVendorSetting'])->name('vendor-settings-store');
Route::post('seller-settings-store', [SettingController::class, 'storeSellerSetting'])->name('seller-settings-store');

Route::prefix('filter')->group(function () {
    Route::get('products', [ProductController::class, 'filter'])->name('filter-products');
});
Route::post('add-selected-vendor-products', [ProductController::class, 'addSelectedVendorProducts'])->name('add-selected-vendor-products');

Route::resource('vendor', VendorController::class);
